import firebase from 'firebase'

const firebaseConfig = {
    apiKey: "AIzaSyD0ioDFuBDiJMVuHzzUHxqTmP2lLDQEZvI",
    authDomain: "zigzag-monitoramento.firebaseapp.com",
    databaseURL: "https://zigzag-monitoramento-default-rtdb.firebaseio.com",
    projectId: "zigzag-monitoramento",
    storageBucket: "zigzag-monitoramento.appspot.com",
    messagingSenderId: "262833968187",
    appId: "1:262833968187:web:c6fedf72e34b35316bf81e",
    measurementId: "G-TRB21FCF69"
};

firebase.initializeApp(firebaseConfig)

export default firebase