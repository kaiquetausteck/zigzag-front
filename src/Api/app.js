const axios = require('axios');

var baseUrl

if(process.env.REACT_APP_STAGE === 'development') {
  baseUrl = 'http://192.168.15.7:1200/';
  //baseUrl = 'https://homolog.text2you.com.br/'
  //baseUrl = 'https://producao.text2you.com.br/'
} else if (process.env.REACT_APP_STAGE === 'homologacao') {
  baseUrl = 'https://homolog.text2you.com.br/';
}  else if (process.env.REACT_APP_STAGE === 'stage') {
  baseUrl = 'https://stage.text2you.com.br/';
} else if (process.env.REACT_APP_STAGE === 'production') {
  baseUrl = 'https://producao.text2you.com.br/'
}

const TOKEN = window.localStorage.getItem('token')

const api = axios.create({
  baseURL: baseUrl,
  headers: {
    'Authorization': 'Bearer '+TOKEN
  }
})

export { api, TOKEN, baseUrl }