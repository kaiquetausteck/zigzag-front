import React, { useEffect, useState } from 'react'
import firebase from '../../firebase'

export default function Monitoramento()
{

    const [brokers, setBrokers] = useState([])

    useEffect(() => {

        getData()

    }, [])

    async function getData()
    {

        let ref = await firebase.database().ref("/brokers")
        ref.on("value", snapshot => {

            setBrokers([...snapshot.val()])
          
        })

    }

    async function newData()
    {

        firebase.database().ref("/brokers").set([
            {
                nome: 'Z-API INSTANCIA 01',
                active: true,
            },
            {
                nome: 'Z-API INSTANCIA 02',
                active: true,
            },
            {
                nome: 'CHATPRO',
                active: true,
            }
        ])

    }

    return(
        <div id="monitoramento">
            
            <div className="brokers">
                { brokers.map((row, key) => 
                <div className={row.active === true ? 'active' : 'false'} key={key}>
                    {row.nome}
                </div>
            )}
            </div>

            <button onClick={() => newData()}>Add</button>

        </div>
    )

}