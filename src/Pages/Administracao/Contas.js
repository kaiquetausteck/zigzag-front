import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min'
import { api } from '../../Api/app'
import IconButton from '../../Components/Icon/IconButton'
import H1Page from '../../Components/Layout/H1Page'
import Paginator from '../../Components/Paginator/Paginator'
import { Button } from '../../Components/TemplateCreation/Widgets/Button/Button'
import messageStore from '../../Store/MessageStore'
//scss
import './administracao.scss'

export default function Contas(props) {

    const [load, setLoad] = useState(true)
    const [data, setData] = useState([])

    const [page, setPage] = useState(1)
    const history = useHistory()

    useEffect(() => {

        getData()

    }, [props])

    async function getData()
    {

        try {

            const response = await api.get('accounts')

            setData(response.data)
            setLoad(false)
            console.log(response)

        } catch ( e ) {

            console.log(e)
            messageStore.addError('Erro ao listar.')

        }

    }

    return(

        <div id="administracao" className="page">

            <H1Page nome="Contas"/>

            <Link to="/adm-contas/new">
                <button type="button" className="button-zig column primary">
                    <IconButton icon="new"/>
                    <span>Nova conta</span>
                </button>
            </Link>

            <table className="table-default">

                <thead>
                    <tr>
                        <th width="10"></th>
                        <th width="10">Conta</th>
                        <th>Usuarios</th>
                    </tr>
                </thead>

                <tbody>
                  
                    { load === false && data.docs.map((row, key) => 
                    
                        <tr key={key}>
                            <td style={{verticalAlign: 'top'}}>
                                <Link to={'/adm-contas/'+row._id}>
                                    <button type="button" className="button-zig secondary">
                                        <span>Editar conta</span>
                                    </button>
                                </Link>
                            </td>
                            <td style={{whiteSpace: 'nowrap', verticalAlign: 'top', paddingTop: '18px'}}>
                                {row.nome}
                            </td>
                            <td>
                                { row.users.map((user, userKey) =>
                                <tr key={userKey}>
                                    <td width="110">
                                        <Link to={'/adm-contas/usuarios/'+user._id}>
                                            <button type="button" className="button-zig secondary">
                                                <span>Editar usuario</span>
                                            </button>
                                        </Link>
                                    </td>
                                    <td align="left">

                                        <div className="perfil-user">
                                            <div className="photo-list" style={{backgroundImage: 'url('+user.foto+')'}}></div>
                                            <span>{user.nome}</span>
                                        </div>

                                    </td>
                                </tr>
                                )}
                                <Link to={'/adm-contas/usuarios/new/'+row._id}>
                                    <button type="button" className="button-zig column primary" style={{marginTop: 10}}>
                                        <IconButton icon="new"/>
                                        <span>Inserir novo usuário</span>
                                    </button>
                                </Link>
                            </td>
                        </tr>

                    )}
                </tbody>

            </table>

            <div className="tfoot">
                <Paginator 
                    range={10} 
                    totalPage={data.pages} 
                    setPage={setPage}
                    page={page}/>
            </div>
            
        </div>

    )

}