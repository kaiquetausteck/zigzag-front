import React, { useEffect, useState } from 'react'
import { api } from '../../Api/app'
import Skeleton from 'react-loading-skeleton'
import { Link } from 'react-router-dom'
import messageStore from '../../Store/MessageStore'
import PreviewTemplate from '../../Components/TemplateCreation/PreviewTemplate/PreviewTemplate'
import H1Page from '../../Components/Layout/H1Page'
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min'
import Modelchatbot from './ModelChatbot'
import IconButton from '../../Components/Icon/IconButton'
import datesFormater from '../../Helper/DatesFormater'
import ModelChatbot from './ModelChatbot'

export default function ChatbotIndex(props){

    const [load, setLoad] = useState(true)
    const [data, setData] = useState([])
    const Model = new ModelChatbot(
        [], 
        () => {}, 
        () => {}
    )

    const history = useHistory()
    

    useEffect(() => {

        getData()

    }, [props])

    async function getData()
    {

        setLoad(true)
        var response = await api.get('chatbots')

        console.log( response )
        setData(...[response.data])
        setLoad( false )

    }
    
    async function handleNew()
    {

        const dataToApi = {
            nome: 'CapiBot',
            estrutura: JSON.stringify(Model.setElementsDefault())
        }

        console.log( dataToApi )

        setLoad(true)
        var response = await api.post('chatbots', dataToApi)

        if ( response.data.error === true ){
            messageStore.addError(response.data.message)
        } else {
            history.push('/chatbot/'+response.data.id)
        }
        
        setLoad(false)

    }

    async function handleDelete(id)
    {

        setLoad(true)
        await api.delete('chatbots/'+id)
        getData()
        setLoad(false)

    }

    return(
        
        <div className="page">

            <div className="h1-button">

                <H1Page nome={"Meus Chatbots"}/>

                <div className="buttons">
                    <button onClick={handleNew} className="button-zig primary">
                        <IconButton icon="new"/>
                        <span>Criar novo Chatbot</span>
                    </button>
                </div>
                
            </div>

            <table className="table-default">

                <thead>
                    <tr>
                        <th colSpan={4}>
                            Nome do Chatbot
                        </th>
                    </tr>
                </thead>

                <tbody>
                    { load === true && 
                        [0,1,2].map((row, key) => 
                            <tr colSpan={4} key={key}>
                                <td><Skeleton/></td>
                            </tr>
                        )
                    }
                    { load === false && data.map((row, key) => 
                        <tr key={key}>
                            <td width="100%">
                                <b>{row.nome}</b><br/>
                                <span>Criado em {datesFormater.dateBR(row.createdAt)}</span>
                            </td>
                            <td>
                                <a className="button-zig primary" onClick={() => history.push("/chatbot/"+row.id)}>
                                    <IconButton icon="edit"/>
                                    <span>Editar</span>
                                </a>
                            </td>
                            <td>
                                <a className="button-zig danger" onClick={() => messageStore.addConfirm('Deseja remover o template "'+row.nome+'"', () => handleDelete(row.id))}>
                                    <IconButton icon="del"/>
                                    <span>Remover</span>
                                </a>
                            </td>
                        </tr>
                    )}
                </tbody>

            </table>

        </div>

    )

}