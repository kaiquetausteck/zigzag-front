import React, {useState, useEffect} from 'react'
import Skeleton from 'react-loading-skeleton'
import { useHistory } from 'react-router-dom'
import { api } from '../../Api/app'
import messageStore from '../../Store/MessageStore'

import "./ContactList.scss"
import EditContact from './EditContact'
import ImportContacts from './ImportContacts'
import ListContact from './ListContact'
import EditColumns from './EditColumns'
import CleanCSV from './CleanCSV'

export default function EditList(props)
{

    const [open, setOpen] = useState(props.open ? props.open : false)
    
    const [page, setPage] = useState(1)
    const [id, setId] = useState(props.id ? props.id : false)
    const [list, setList] = useState({})
    const [columns, setColumns] = useState([])
    const [contacts, setContacts] = useState([])
    const [nameList, setNameList] = useState('')
    const [tab, setTab] = useState('list')
    const [contact, setContact] = useState({})
    const [redirect, setRedirect] = useState(null)

    useEffect(() => {

        console.log('alterado', columns, id)
        props.callbackChangeColumns(columns, id)

    }, [columns, tab])

    useEffect(() => {

        console.log(id)

        if ( id ){

            api.get('contactLists/'+id).then( response => {

                setList({...response.data})
                setNameList(response.data.lista)
                setColumns(response.data.colunas)
                setTab('list')

            })

        }

    }, [open, id])

    function handleOpen()
    {

        setOpen(true)

    }

    function handleSaveName(e)
    {

        e.preventDefault()

        api.put('contactLists/'+list._id, {
            lista: nameList,
            colunas: columns
        }).then( response => {

            messageStore.addSuccess('Nome alterado com sucesso.')
            props.callbackChangeName( list._id, nameList )

        }).catch(e => {

            messageStore.addError('Não foi possivel alterar o nome.')

        })

    }

    function handleNewList(e)
    {
        
        e.preventDefault()

        api.post('contactLists', {
            lista: nameList,
            tipo: props.send,
            colunas: [props.send === 'Email' ? 'Email' : 'Celular','Nome']
        }).then( response => {

            if ( response.data.error === true ){
                messageStore.addError(response.data.message)
            } else {

                messageStore.addSuccess('Lista salva.')
                props.callbackNewList(response.data.result)
                setOpen(false)
                setNameList('')

            }
            

        }).catch( e => {

            messageStore.addError('Não foi possivel criar a lista.')

        })

    }

    function handleClose()
    {

        if ( open === true ){

            var element = document.getElementById("contact-list")
                element.classList.add("closed")

            setTimeout(function(){
                setOpen(false)
            }, 900)

        }

    }

    async function handleEditColumn( col )
    {

        setColumns([...col])

        const response = await api.put('contactLists/'+list._id, {
            colunas: col
        })

        console.log( response )

    }

    return(

        <>

            <div type="button" className={`button-edit-list ${props.className}`} onClick={() => open === true ? handleClose(false) : handleOpen(false)}>
                {props.children ? props.children : <button type="button" className="button-zig secondary">Editar</button>}
            </div>

            { open === true &&
                <div id="contact-list" className="modal-contact">

                    <button type="button" className="bg-close" onClick={() => open === true ? handleClose(false) : handleOpen(false)}></button>

                    <div className="modal" id="contact-list-modal">

                        <button onClick={() => setOpen(false)} className="close-modal">X</button>

                        <form className="head" onSubmit={list._id ? handleSaveName : handleNewList}>
                            <input className="input-default" placeholder="Nome da lista" defaultValue={nameList} onChange={(e) => setNameList(e.target.value)}/>
                            <button>{list._id ? 'Salvar nome' : 'Salvar'}</button>
                        </form>

                        { id &&
                        <>

                            <div className="button">

                                <button type="button" onClick={() => setTab('list')} className={tab === 'list' ? 'button-zig secondary' : 'button-zig neutral'}>Contatos</button>

                                <button type="button" onClick={() => setTab('add')} className={tab === 'add' ? 'button-zig secondary' : 'button-zig neutral'}>Adicionar contatos avulsos</button>

                                <button type="button" onClick={() => setTab('import')} className={tab === 'import' ? 'button-zig secondary' : 'button-zig neutral'}>Importar contatos</button>

                                <button type="button" onClick={() => setTab('columns')} className={tab === 'columns' ? 'button-zig secondary' : 'button-zig neutral'}>Editar colunas</button>

                                <button type="button" onClick={() => setTab('clean')} className={tab === 'higienizar' ? 'button-zig secondary' : 'button-zig neutral'}>Higienizar CSV</button>

                            </div>

                            <div className="content">

                                { tab === 'list' &&
                                    <ListContact
                                        open={tab === 'list' ? true : false}
                                        page={page}
                                        setPage={setPage}
                                        id={id}
                                        columns={columns}
                                        setTab={setTab}
                                        setContact={setContact}
                                        send={props.send}
                                    />
                                }

                                { tab === 'add' &&
                                    <EditContact
                                        open={tab === 'add' ? true : false}
                                        list={list}
                                        nameList={nameList}
                                        columns={columns}
                                        handleNewContact={handleEditColumn}
                                        handleEditColumn={handleEditColumn}
                                        setTab={setTab}
                                        send={props.send}
                                    />
                                }

                                { tab === 'edit' &&
                                    <EditContact
                                        open={tab === 'edit' ? true : false}
                                        setTab={setTab}
                                        list={list}
                                        nameList={nameList}
                                        columns={columns}
                                        contact={contact}
                                        handleEditColumn={handleEditColumn}
                                        send={props.send}
                                    />
                                }

                                { tab === 'columns' &&
                                    <EditColumns
                                        open={tab === 'columns' ? true : false}
                                        setTab={setTab}
                                        list={list}
                                        nameList={nameList}
                                        columns={columns}
                                        contact={contact}
                                        handleEditColumn={handleEditColumn}
                                        redirect={redirect}
                                        setRedirect={setRedirect}
                                        send={props.send}
                                    />
                                }
                                
                                <ImportContacts
                                    open={tab === 'import' ? true : false}
                                    setTab={setTab}
                                    list={list}
                                    nameList={nameList}
                                    columns={columns}
                                    contact={contact}
                                    handleEditColumn={handleEditColumn}
                                    redirect={redirect}
                                    setRedirect={setRedirect}
                                    send={props.send}
                                />
                                
                                <CleanCSV
                                    open={tab === 'clean' ? true : false}
                                    setTab={setTab}
                                    list={list}
                                    nameList={nameList}
                                    columns={columns}
                                    contact={contact}
                                    handleEditColumn={handleEditColumn}
                                    redirect={redirect}
                                    setRedirect={setRedirect}
                                    send={props.send}
                                />

                            </div>

                        </>
                        }

                    </div>

                </div>
            }
        </>

    )

}