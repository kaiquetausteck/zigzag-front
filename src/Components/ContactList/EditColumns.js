import React, {useState, useEffect} from 'react'
import { api } from '../../Api/app'
import messageStore from '../../Store/MessageStore'
import InputDefault from '../Form/InputDefault'
import FormDefault from '../Form/FormDefault'
import InputRow from '../Form/InputRow'
import IconButton from '../Icon/IconButton'

import "./ContactList.scss"

export default function EditColumns(props)
{

    const open = props.open
    const [load, setLoad] = useState(true)
    const [columns, setColumns] = useState([])
    const [values, setValues] = useState({
        contactListId: props.list._id
    })

    useEffect(() => {

        setColumns([...props.columns])

        if ( props.contact ){
            console.log( props.contact )
            setValues( {...props.contact} )
        }

    }, [])

    function handleNewColumn()
    {

        try {
            
            if ( columns.length >= 20 ) throw "O máximo de colunas permitidas são 20."

            columns.push('Coluna '+(columns.length + 1))
            setColumns([...columns])

        } catch (e) {

            messageStore.addError(e)

        }

    }

    function handleDeleteColumn(name)
    {

        const list = columns.filter( obj => 
            obj !== name
        )

        console.log( list )

        setColumns([...list])

    }

    function handleChangeColumn(name, value)
    {

        columns[name] = value

        setColumns([...columns])

        console.log( columns )

    }

    async function handleConfirm(e)
    {

        try {

            e.preventDefault()
            props.handleEditColumn( columns )
            messageStore.addSuccess('Colunas salva com sucesso.')

            if ( props.redirect ) {
                props.setTab(props.redirect)
                props.setRedirect(false)
            }

        } catch (err) {

            console.log(err)
            messageStore.addError('Algo deu errado...')

        }

    }


    return(

        <form onSubmit={handleConfirm} style={{display: open ? 'block' : 'none'}}>

            <div className="overflow">

                <table className="table-default">

                    <thead>
                        <tr>
                            <th>
                                Coluna
                            </th>
                        </tr>
                    </thead>

                    <tbody>

                    {columns.map((coluna, indice) =>
                        <tr>
                            <th style={{paddingRight: 20}}>
                                <InputDefault readOnly={coluna === 'Celular' || coluna === 'Email' ? 'true' : false} onChange={handleChangeColumn} name={indice} value={coluna}/>
                            </th>
                        </tr>
                        )}
                    
                    </tbody>

                    <tfoot>
                        <tr>
                            <td>
                                <button type="button" className="button-zig secondary" style={{flexDirection: 'row'}} onClick={handleNewColumn}>
                                    <IconButton icon="new"/>
                                    <span style={{marginLeft: '10px'}}>Adicionar novo campo</span>
                                </button>
                            </td>
                        </tr>
                    </tfoot>

                </table>

            </div>

            <div className="tfoot">
                <button className="button-zig primary" style={{flexDirection: 'row'}}>
                    <IconButton icon="save"/>
                    <span style={{marginLeft: '10px'}}>Confirmar</span>
                </button>
            </div>
            
        </form>

    )

}