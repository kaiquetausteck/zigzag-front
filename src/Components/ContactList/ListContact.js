import React, {useState, useEffect} from 'react'
import Skeleton from 'react-loading-skeleton'
import { useHistory } from 'react-router-dom'
import { api } from '../../Api/app'
import messageStore from '../../Store/MessageStore'
import IconButton from '../Icon/IconButton'
import Paginator from '../Paginator/Paginator'

import "./ContactList.scss"

export default function ListContact(props)
{

    const id = props.id
    const columns = props.columns
    const page = props.page
    const open = props.open

    const [load, setLoad] = useState(true)
    const [contacts, setContacts] = useState([])

    useEffect(() => {

        setContacts([])

        setLoad(true)

        getContacts()

    }, [open, page])

    async function getContacts()
    {

        api.get('contacts?contactListId='+id+'&page='+page).then( response => {

            setContacts( response.data )
            setLoad( false )

        })

    }

    async function handleDelContact(id)
    {

        await api.delete('contacts/'+id)
        getContacts()

        const docs = contacts.docs.filter( obj => 
            obj._id !== id
        )

        contacts.docs = docs
        setContacts({...contacts})

    }

    function handleEdit( contact )
    {

        props.setTab('edit')
        props.setContact(contact)
        
    }

    return(

        <div style={{display: open ? 'block' : 'none'}}>

            <div className="overflow">
                <table className="table-default">

                    <thead>
                        <tr>
                            <th width="10"></th>
                            <th width="10"></th>
                            <th width="10">Origem</th>
                            {  props.columns.length !== 0 && load === false && props.columns.map((row, key) =>
                                <th>
                                    {row}
                                </th>
                            )}
                        </tr>
                    </thead>

                    <tbody>

                        { load === true &&
                        <tr>
                            <td colSpan={columns.length + 3}>
                                <Skeleton/>
                            </td>
                        </tr>
                        }

                        {  load === false && contacts.docs.length == 0 &&
                            <tr>
                                <td colSpan={columns.length + 3}>Nenhum contato encontrado.</td>
                            </tr>
                        }

                        { columns.length !== 0 && load === false && contacts.docs.map((row, key) =>
                        <tr key={key}>
                            <td>
                                <button type="button" className="button-zig danger" onClick={() => messageStore.addConfirm('Deseja realmente remover esse contato?', () => handleDelContact(row._id))}>
                                    <IconButton icon="del"/>
                                </button>
                            </td>
                            <td>
                                <button type="button" className="button-zig primary" onClick={() => handleEdit(row)}>
                                    <IconButton icon="edit"/>
                                </button>
                            </td>
                            <td>
                                { row.origem }
                            </td>
                            {columns.map((coluna, indice) =>
                                <td>
                                    {row['field'+(indice + 1)]}
                                </td>
                            )}
                        </tr>
                        )}

                    </tbody>

                </table>
            </div>

            <div className="tfoot">
                <Paginator 
                    range={4} 
                    totalPage={contacts.pages} 
                    setPage={props.setPage}
                    page={page}/>
            </div>

        </div>

    )

}