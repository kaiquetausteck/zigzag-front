import React from "react"

export default function MenuEdit({
    onSubmit, edition, selected, elements, setEdition
}) {

    return (
        <form onSubmit={onSubmit} className={`edit-dialog ${edition ? 'opened' : 'closed'}`}>
        
            <div className="head">

            <input 
                onChange={(e) => onChangeNameDialog(e.target.value)}
                value={selected.params.nameDialog}
            />

            <button onClick={() => {setEdition(false); setSelected(selectedDefault)}}>
                fechar
            </button>

            </div>

            <select>
            { elements.filter(obj => obj.data ).map((row, key) =>
                <option key={key}>{row.params.nameDialog}</option>
            )}
            </select>

        </form>
    )

}