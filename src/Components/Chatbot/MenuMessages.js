import React, { useEffect, useRef, useState } from 'react'
import IconButton from '../Icon/IconButton'
import ToggleSwitch from '../ToggleSwitch/ToggleSwitch'
import ContentEditable from 'react-contenteditable'
import { Scrollbars } from 'react-custom-scrollbars'

export default function MenuMessages({
    elements,
    selected,
    setSelected,
    Model,
}) {

    const emojis = '😀 😃 😄 😁 😆 😅 😂 🤣 😊 😇 🙂 🙃 😉 😌 😍 🥰 😘 😗 😙 😚 😋 😛 😝 😜 🤪 🤨 🧐 🤓 😎 🤩 🥳 😏 😒 😞 😔 😟 😕 🙁 😣 😖 😫 😩 🥺 😢 😭 😤 😠 😡 🤬 🤯 😳 🥵 🥶 😱 😨 😰 😥 😓 🤗 🤔 🤭 🤫 🤥 😶 😐 😑 😬 🙄 😯 😦 😧 😮 😲 🥱 😴 🤤 😪 😵 🤐 🥴 🤢 🤮 🤧 😷 🤒 🤕 🤑 🤠 😈 👿 👹 👺 🤡 💩 👻 💀 ☠️ 👽 👾 🤖 🎃 😺 😸 😹 😻 😼 😽 🙀 😿 😾 👋 🤚 🖐 ✋ 🖖 👌 🤏 ✌️ 🤞 🤟 🤘 🤙 👈 👉 👆 🖕 👇 ☝️ 👍 👎 ✊ 👊 🤛 🤜 👏 🙌 👐 🤲 🤝 🙏 ✍️ 💅 🤳 💪 🦾 🦵 🦿 🦶 👣 👂 🦻 👃 👶 👧 🧒 👦 👩 🧑 👨 👩‍🦱 🧑‍🦱 👨‍🦱 👩‍🦰 🧑‍🦰 👨‍🦰 👱‍♀️ 👱 👱‍♂️ 👩‍🦳 🧑‍🦳 👨‍🦳 👩‍🦲 🧑‍🦲 👨‍🦲 🧔 👵 🧓 👴 👲 👳‍♀️ 👳 👳‍♂️ 🧕 👮‍♀️ 👮 👮‍♂️ 👷‍♀️ 👷 👷‍♂️ 💂‍♀️ 💂 💂‍♂️ 🕵️‍♀️ 🕵️ 🕵️‍♂️ 👩‍⚕️ 🧑‍⚕️ 👨‍⚕️ 👩‍🌾 🧑‍🌾 👨‍🌾 👩‍🍳 🧑‍🍳 👨‍🍳 👩‍🎓 🧑‍🎓 👨‍🎓 👩‍🎤 🧑‍🎤 👨‍🎤 👩‍🏫 🧑‍🏫 👨‍🏫 👩‍🏭 🧑‍🏭 👨‍🏭 👩‍💻 🧑‍💻 👨‍💻 👩‍💼 🧑‍💼 👨‍💼 👩‍🔧 🧑‍🔧 👨‍🔧 👩‍🔬 🧑‍🔬 👨‍🔬 👩‍🎨 🧑‍🎨 👨‍🎨 👩‍🚒 🧑‍🚒 👨‍🚒 👩‍✈️ 🧑‍✈️ 👨‍✈️ 👩‍🚀 🧑‍🚀 👨‍🚀 👩‍⚖️ 🧑‍⚖️ 👨‍⚖️ 👰‍♀️ 👰 👰‍♂️ 🤵‍♀️ 🤵 🤵‍♂️ 👸 🤴 🦸‍♀️ 🦸 🦸‍♂️ 🦹‍♀️ 🦹 🦹‍♂️ 🤶 🧑‍🎄 🎅 🧙‍♀️ 🧙 🧙‍♂️ 🧝‍♀️ 🧝 🧝‍♂️ 🧛‍♀️ 🧛 🧛‍♂️ 🧟‍♀️ 🧟 🧟‍♂️ 🧞‍♀️ 🧞 🧞‍♂️ 🧜‍♀️ 🧜 🧜‍♂️ 🧚‍♀️ 🧚 🧚‍♂️ 👼 🤰 🤱 👩‍🍼 🧑‍🍼 👨‍🍼 🙇‍♀️ 🙇 🙇‍♂️ 💁‍♀️ 💁 💁‍♂️ 🙅‍♀️ 🙅 🙅‍♂️ 🙆‍♀️ 🙆 🙆‍♂️ 🙋‍♀️ 🙋 🙋‍♂️ 🧏‍♀️ 🧏 🧏‍♂️ 🤦‍♀️ 🤦 🤦‍♂️ 🤷‍♀️ 🤷 🤷‍♂️ 🙎‍♀️ 🙎 🙎‍♂️ 🙍‍♀️ 🙍 🙍‍♂️ 💇‍♀️ 💇 💇‍♂️ 💆‍♀️ 💆 💆‍♂️ 🧖‍♀️ 🧖 🧖‍♂️ 💅 🤳 💃 🕺 👯‍♀️ 👯 👯‍♂️ 🕴 👩‍🦽 🧑‍🦽 👨‍🦽 👩‍🦼 🧑‍🦼 👨‍🦼 🚶‍♀️ 🚶 🚶‍♂️ 👩‍🦯 🧑‍🦯 👨‍🦯 🧎‍♀️ 🧎 🧎‍♂️ 🏃‍♀️ 🏃 🏃‍♂️ 🧍‍♀️ 🧍 🧍‍♂️ 👭 🧑‍🤝‍🧑 👬 👫 👩‍❤️‍👩 💑 👨‍❤️‍👨 👩‍❤️‍👨 👩‍❤️‍💋‍👩 💏 👨‍❤️‍💋‍👨 👩‍❤️‍💋‍👨 👪 👨‍👩‍👦 👨‍👩‍👧 👨‍👩‍👧‍👦 👨‍👩‍👦‍👦 👨‍👩‍👧‍👧 👨‍👨‍👦 👨‍👨‍👧 👨‍👨‍👧‍👦 👨‍👨‍👦‍👦 👨‍👨‍👧‍👧 👩‍👩‍👦 👩‍👩‍👧 👩‍👩‍👧‍👦 👩‍👩‍👦‍👦 👩‍👩‍👧‍👧 👨‍👦 👨‍👦‍👦 👨‍👧 👨‍👧‍👦 👨‍👧‍👧 👩‍👦 👩‍👦‍👦 👩‍👧 👩‍👧‍👦 👩‍👧‍👧 🗣 👤 👥'

    const [message, setMessage] = useState('')

    const [messageReference, setMessageReference] = useState({})

    const refContentEditable = useRef()

    const [boxEmoji, setBoxEmoji] = useState(false)

    const [sending, setSending] = useState(false)

    useEffect(() => {

        handleMessageEdit()

    }, [sending])

    function handleMessageUserRequired(e)
    {

        selected.params.messageUser.required = e
        Model.onEditElement(selected)
        setSelected({...selected})

    }

    function handleMessageUserVariable(e)
    {

        selected.params.messageUser.variable = e
        Model.onEditElement(selected)
        setSelected({...selected})

    }

    function handleNewMessage()
    {

        selected.params.messages.push({
            message: 'Nova mensagem, clique em editar.'
        })
        Model.onEditElement(selected)
        setSelected({...selected})

    }

    function handleRemoveMessage(indice)
    {

        const newMessages = []

        selected.params.messages.map((row, key) => {
            if ( key !== indice ) {
                newMessages.push(row)
            }
        })

        selected.params.messages = newMessages
        Model.onEditElement(selected)
        setSelected({...selected})

    }

    function handleMessageEditing(indice)
    {

        selected.params.messages.map((row, key) => {
            if ( key === indice ) {
                setMessage(row.message)
                setMessageReference({
                    message: row.message,
                    key: key,
                })
            }
        })

    }

    function insertEmoji(val) 
    {

        const step = refContentEditable.current

        step.focus()

        var selection = window.getSelection()

        for (var i = 0; i < step; i += 1) {

            selection.modify('extend', 'backward', 'character')

        }

        document.execCommand('insertText', false, val)

    }

    function replaceEmoji(text)
    {

        text = text.replace(':D', '😁')
        text = text.replace(':(', '🙁')
        text = text.replace(':)', '😀')
        text = text.replace(';(', '😢')
        text = text.replace(';)', '😉')
        text = text.replace('<3', '❤️')
        return text
        
    }

    function checkSubmit(e)
    {

        if ( !e.shiftKey && e.key === 'Enter' ){
            
            setSending(true)

        }

    }

    function handleMessageEdit()
    {
        
        if ( sending === true ) {

            const newMessages = []
            
            selected.params.messages.map((row, key) => {

                if ( key === messageReference.key ) {
                    row.message = message
                }

                newMessages.push(row)

            })

            selected.params.messages = newMessages

            Model.onEditElement(selected)
            setSelected({...selected})

            setMessage('')
            setMessageReference({})
            setSending(false)

        }

    }

    return(
        <div className="messages">


            { selected.typeDialog === 'start' &&
                <div className="tutorialTypeDialog">
                    <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><g fill="none"><circle cx="12" cy="12" r="10" stroke="#626262" stroke-width="2"/><path d="M11.5 7h.5" stroke="#626262" stroke-width="2" stroke-linecap="round"/><path d="M10 11h2v5" stroke="#626262" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M10 16h4" stroke="#626262" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></g><rect x="0" y="0" width="24" height="24" fill="rgba(0, 0, 0, 0)" /></svg>
                    <div>
                        <h2>Início</h2>
                        <p>A conversa do seu chatbot sempre inicia através da Entrada do usuário. Crie novos blocos para adicionar conteúdos e desenvolva uma conversa com seu cliente.</p>
                    </div>
                </div>
            }

            { selected.typeDialog === 'end' && 
                <div className="tutorialTypeDialog">
                    <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><g fill="none"><circle cx="12" cy="12" r="10" stroke="#626262" stroke-width="2"/><path d="M11.5 7h.5" stroke="#626262" stroke-width="2" stroke-linecap="round"/><path d="M10 11h2v5" stroke="#626262" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M10 16h4" stroke="#626262" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></g><rect x="0" y="0" width="24" height="24" fill="rgba(0, 0, 0, 0)" /></svg>
                    <div>
                        <h2>Exceções</h2>
                        <p>O bloco de exceções vai te ajudar a tratar condições padrão do seu chatbot.</p>
                    </div>
                </div>
            }

            { selected.typeDialog === 'attendance' &&
                <div className="tutorialTypeDialog">
                    <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><g fill="none"><circle cx="12" cy="12" r="10" stroke="#626262" stroke-width="2"/><path d="M11.5 7h.5" stroke="#626262" stroke-width="2" stroke-linecap="round"/><path d="M10 11h2v5" stroke="#626262" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M10 16h4" stroke="#626262" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></g><rect x="0" y="0" width="24" height="24" fill="rgba(0, 0, 0, 0)" /></svg>
                    <div>
                        <h2>Atendimento</h2>
                        <p>O bloco de atendimento, sempre levará a algum operador e finalizará o fluxo do bot.</p>
                    </div>
                </div>
            }

            <section>

                <div className="messages-bot">

                    { selected.params.messages.map((row, key) => 
                        <div className="config-message">

                            <div className={`message bot`}>
                                { row.message }
                            </div>

                            <div className="buttons">
                                <button type="button" className="removeMessage button-circle" onClick={() => handleRemoveMessage(key)}>
                                    <IconButton icon="del"/>
                                </button>

                                <button type="button" className="editMessage button-circle" onClick={() => handleMessageEditing(key)}>
                                    <svg xmlns="http://www.w3.org/2000/svg"  aria-hidden="true" focusable="false" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><path d="M12 2C6.486 2 2 5.589 2 10c0 2.908 1.898 5.515 5 6.934V22l5.34-4.005C17.697 17.852 22 14.32 22 10c0-4.411-4.486-8-10-8zm0 14h-.333L9 18v-2.417l-.641-.247C5.67 14.301 4 12.256 4 10c0-3.309 3.589-6 8-6s8 2.691 8 6s-3.589 6-8 6z" fill="#626262"/><path d="M8.503 11.589v1.398h1.398l3.87-3.864l-1.399-1.398z" fill="#626262"/><path d="M14.43 8.464l-1.398-1.398l1.067-1.067l1.398 1.398z" fill="#626262"/><rect x="0" y="0" width="24" height="24" fill="rgba(0, 0, 0, 0)" /></svg>
                                </button>
                            </div>


                        </div>
                    )}

                    { selected.typeDialog !== 'start' && selected.typeDialog !== 'end' && selected.typeDialog !== 'attendance' &&
                        <div className="contentNewMessage">
                            <span>Adicionar nova mensagem</span>
                            <button type="button" className="newMessage button-circle" onClick={handleNewMessage}>
                                <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024"><path d="M696 480H544V328c0-4.4-3.6-8-8-8h-48c-4.4 0-8 3.6-8 8v152H328c-4.4 0-8 3.6-8 8v48c0 4.4 3.6 8 8 8h152v152c0 4.4 3.6 8 8 8h48c4.4 0 8-3.6 8-8V544h152c4.4 0 8-3.6 8-8v-48c0-4.4-3.6-8-8-8z" fill="#626262"/><path d="M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448s448-200.6 448-448S759.4 64 512 64zm0 820c-205.4 0-372-166.6-372-372s166.6-372 372-372s372 166.6 372 372s-166.6 372-372 372z" fill="#626262"/><rect x="0" y="0" width="1024" height="1024" fill="rgba(0, 0, 0, 0)" /></svg>
                            </button>
                        </div>
                    }

                </div>

                <div className="messages-user">
                    { selected.params.messageUser.required && 
                        <div className="config-message">
                            <div className={`message user`}>
                                Resposta do usuário
                            </div>

                            <div className="variable config-message-user">
                                <label onClick={() => handleMessageUserVariable(selected.params.messageUser.variable === null ? '' : null)}>
                                    <ToggleSwitch
                                        value={selected.params.messageUser.variable !== null ? 1 : 0}
                                        setValue={() => {}}
                                    />
                                    <span>
                                        Salvar resposta em variavel
                                    </span>
                                </label>

                                { selected.params.messageUser.variable !== null && 
                                    <div className="form">
                                        <input value={selected.params.messageUser.variable} onChange={(e) => handleMessageUserVariable(e.target.value)} placeholder="Nome da variavel"/>
                                    </div>
                                }
                            </div>

                        </div>
                    }
                </div>

                { selected.typeDialog !== 'start' && selected.typeDialog !== 'end' && selected.typeDialog !== 'attendance' &&
                    <div className="config-message-user" onClick={() => handleMessageUserRequired(!selected.params.messageUser.required)}>
                        <ToggleSwitch value={selected.params.messageUser.required === true ? 1 : 0} setValue={(e) => {}} />
                        <span className="text">Aguardar resposta do usuario</span>

                    </div>
                }

            </section>

            { messageReference.message && 
                <div className="editing-message">

                    <div className="emojis">
                        
                        <div className={`list ${boxEmoji ? 'active' : 'hidden'}`}>
                            <Scrollbars
                                style={{ width: '100%', height: '100%' }}
                            >

                                { emojis.split(' ').map((row, key) =>
                                    <button type="button" onClick={() => insertEmoji(row)} key={key}>
                                        {row}
                                    </button>
                                )}

                            </Scrollbars>
                        </div>

                    </div>

                    <div className="visible">

                        <div className="emojis">

                            <div className={`buttons ${boxEmoji ? 'active' : ''}`}>
                                
                                <button type="button" className="close" onClick={() => setBoxEmoji(false)}>
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="currentColor" d="M19.1 17.2l-5.3-5.3 5.3-5.3-1.8-1.8-5.3 5.4-5.3-5.3-1.8 1.7 5.3 5.3-5.3 5.3L6.7 19l5.3-5.3 5.3 5.3 1.8-1.8z"></path></svg>
                                </button>

                                <button type="button" className="open" onClick={() => setBoxEmoji(true)}>
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="currentColor" d="M9.153 11.603c.795 0 1.439-.879 1.439-1.962s-.644-1.962-1.439-1.962-1.439.879-1.439 1.962.644 1.962 1.439 1.962zm-3.204 1.362c-.026-.307-.131 5.218 6.063 5.551 6.066-.25 6.066-5.551 6.066-5.551-6.078 1.416-12.129 0-12.129 0zm11.363 1.108s-.669 1.959-5.051 1.959c-3.505 0-5.388-1.164-5.607-1.959 0 0 5.912 1.055 10.658 0zM11.804 1.011C5.609 1.011.978 6.033.978 12.228s4.826 10.761 11.021 10.761S23.02 18.423 23.02 12.228c.001-6.195-5.021-11.217-11.216-11.217zM12 21.354c-5.273 0-9.381-3.886-9.381-9.159s3.942-9.548 9.215-9.548 9.548 4.275 9.548 9.548c-.001 5.272-4.109 9.159-9.382 9.159zm3.108-9.751c.795 0 1.439-.879 1.439-1.962s-.644-1.962-1.439-1.962-1.439.879-1.439 1.962.644 1.962 1.439 1.962z"></path></svg>
                                </button>

                            </div>

                        </div>

                        <div className="input">
                        
                            <ContentEditable
                                id="input-send"
                                innerRef={refContentEditable}
                                html={message}
                                disabled={false}
                                onChange={(e) => setMessage(replaceEmoji(e.currentTarget.innerText))}
                                onKeyDown={checkSubmit}
                            />

                            { !message && <span>Digite uma mensagem</span> }
                            
                        </div>

                        <button className="removeMessage button-circle" onClick={() => setMessageReference({})}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 16 16"><g fill="#626262"><path fill-rule="evenodd" clip-rule="evenodd" d="M8 8.707l3.646 3.647l.708-.707L8.707 8l3.647-3.646l-.707-.708L8 7.293L4.354 3.646l-.707.708L7.293 8l-3.646 3.646l.707.708L8 8.707z"/></g><rect x="0" y="0" width="16" height="16" fill="rgba(0, 0, 0, 0)" /></svg>
                        </button>

                    </div>

                </div>
            }

        </div>
    )

}