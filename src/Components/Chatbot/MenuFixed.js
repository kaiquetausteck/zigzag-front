import React, { useState } from "react"
import { api } from "../../Api/app"
import messageStore from "../../Store/MessageStore"

export default function MenuFixed({
    Model, handleOnSave, name, id
}) {

    const [phones, setPhones] = useState([])

    const [phone, setPhone] = useState('')

    function validatePhone(phone) {

        phone = phone.replace(' ', '').replace(' ', '').replace(' ', '').replace('(', '').replace(')', '').replace('-', '')

        var regex = new RegExp('^((1[1-9])|([2-9][0-9]))((3[0-9]{3}[0-9]{4})|(9[0-9]{3}[0-9]{5}))$')
        return regex.test(phone)

    }

    async function sendAvulsos()
    {

        try {

            setLoad(true)

            await handleOnSave()

            if ( phones.length === 0 ) throw "Nenhum destinatário encontrado."

            const response = await api.post('send/whatsapp', {
                mensagem: 'Responda essa mensagem para iniciar o fluxo do '+name,
                destinatarios: phones,
                imagem: '',
                templateId: '',
                chatbotId: id,
                frontend: window.location.protocol+'//'+window.location.host+'/t/',
            })

            messageStore.addSuccess('Enviado com sucesso.')

            setLoad(false)

        } catch (e) {

            messageStore.addError(e)
            setLoad(false)

        }

    }

    function handlePhones(e)
    {

        const value = e.target.value.replace(' ', '').replace(' ', '').replace(' ', '').replace('(', '').replace(')', '').replace('-', '')

        setPhone(value)
        setPhones(value.split(','))

    }

    const [load, setLoad] = useState(false)

    const [modalTest, setModalTest] = useState(false)

    return (
        <div className="menu-fixed">

            <button className="newDialog" onClick={() => Model.handleNewElement()}>
                <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024"><path d="M696 480H544V328c0-4.4-3.6-8-8-8h-48c-4.4 0-8 3.6-8 8v152H328c-4.4 0-8 3.6-8 8v48c0 4.4 3.6 8 8 8h152v152c0 4.4 3.6 8 8 8h48c4.4 0 8-3.6 8-8V544h152c4.4 0 8-3.6 8-8v-48c0-4.4-3.6-8-8-8z" fill="#626262"/><path d="M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448s448-200.6 448-448S759.4 64 512 64zm0 820c-205.4 0-372-166.6-372-372s166.6-372 372-372s372 166.6 372 372s-166.6 372-372 372z" fill="#626262"/><rect x="0" y="0" width="1024" height="1024" fill="rgba(0, 0, 0, 0)" /></svg>
                <span>Novo diálogo</span>
            </button>

            <div className="menu-relative">
                <button className="testFlow" onClick={() => setModalTest(!modalTest)}>
                    <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><path d="M13.293 2.707l.818.818L3.318 14.318C2.468 15.168 2 16.298 2 17.5s.468 2.332 1.318 3.183C4.169 21.532 5.299 22 6.5 22s2.331-.468 3.182-1.318L20.475 9.889l.818.818l1.414-1.414l-8-8l-1.414 1.414zm3.182 8.354l-2.403-2.404l-1.414 1.414l2.403 2.404l-1.414 1.415l-.99-.99l-1.414 1.414l.99.99l-1.415 1.415l-2.403-2.404L7 15.728l2.403 2.404l-1.136 1.136c-.945.944-2.59.944-3.535 0C4.26 18.795 4 18.168 4 17.5s.26-1.295.732-1.768L15.525 4.939l3.535 3.535l-2.585 2.587z" fill="#626262"/><rect x="0" y="0" width="24" height="24" fill="rgba(0, 0, 0, 0)" /></svg>
                    <span>Testar fluxo</span>
                </button>
                
                <div className={`send-phones-fixed ${modalTest === true ? 'active' : 'inactive'}`}>

                    <p>Digite celulares separados por virgula, para receber o fluxo em seu celular e poder testar.</p>

                    <div className="form">
                        <input onChange={(e) => handlePhones(e)} type="text" value={phone} className="input-default" placeholder="Celulares separados por virgula"/>
                        <button type="button" onClick={sendAvulsos}>
                            {load === true ? <i className="fa fa-spin fa-spinner"></i> : 'Enviar'}
                        </button>
                    </div>

                    <div className="phones">
                        { phones.map((row, key) => 
                            <span className={`phone-number ${validatePhone(row) ? 'valid' : 'invalid'}`}>
                                {validatePhone(row) ? <i className="fa fa-check"/> : <i className="fa fa-times"/>}
                                {row}
                            </span>
                        )}
                    </div>
                </div>
            </div>

            <button className="resetFlow" onClick={() => Model.setElementsDefault()}>
                <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><g fill="none"><path d="M6.78 2.72a.75.75 0 0 1 0 1.06L4.56 6h8.69a7.75 7.75 0 1 1-7.75 7.75a.75.75 0 0 1 1.5 0a6.25 6.25 0 1 0 6.25-6.25H4.56l2.22 2.22a.75.75 0 1 1-1.06 1.06l-3.5-3.5a.75.75 0 0 1 0-1.06l3.5-3.5a.75.75 0 0 1 1.06 0z" fill="#626262"/></g><rect x="0" y="0" width="24" height="24" fill="rgba(0, 0, 0, 0)" /></svg>
                <span>Resetar</span>
            </button>

            <button className="saveFlow" onClick={() => handleOnSave()}>
                <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 20 20"><g fill="none"><path d="M3 5a2 2 0 0 1 2-2h8.379a2 2 0 0 1 1.414.586l1.621 1.621A2 2 0 0 1 17 6.621V15a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5zm2-1a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1v-4.5A1.5 1.5 0 0 1 6.5 10h7a1.5 1.5 0 0 1 1.5 1.5V16a1 1 0 0 0 1-1V6.621a1 1 0 0 0-.293-.707l-1.621-1.621A1 1 0 0 0 13.379 4H13v2.5A1.5 1.5 0 0 1 11.5 8h-4A1.5 1.5 0 0 1 6 6.5V4H5zm2 0v2.5a.5.5 0 0 0 .5.5h4a.5.5 0 0 0 .5-.5V4H7zm7 12v-4.5a.5.5 0 0 0-.5-.5h-7a.5.5 0 0 0-.5.5V16h8z" fill="#626262"/></g><rect x="0" y="0" width="20" height="20" fill="rgba(0, 0, 0, 0)" /></svg>
                <span>Salvar</span>
            </button>

        </div>
    )

}