import React, { useEffect, useState } from 'react'

import './inputMaskTropa.scss'

export default function InputMaskTropa({
    onChange,
    placeholder,
    value,
    type,
}){

    const [valueFormatted, setValueFormatted] = useState('')
    const [placeholderType, setPlaceholderType] = useState('')

    useEffect(() => {
        
        if ( type === 'cpf' ) typeCPF( value )
        if ( type === 'cnpj' ) typeCPF( value )
        if ( type === 'email' ) typeCPF( value )
        if ( type === 'cellphone' ) typeCPF( value )
        if ( type === 'phone' ) typeCPF( value )
        if ( type === 'date' ) typeCPF( value )
        if ( type === 'datetime' ) typeCPF( value )
        if ( type === 'time' ) typeCPF( value )

    }, [value])

    function handleOnChange( e )
    {

        onChange(e.target.value)

    }
    


    function typeCPF( value )
    {

        console.log('')

        let mask = ['_','_','_','.','_','_','_','.','_','_','_','-','_','_']
        setPlaceholderType(mask.join(''))
        let replace = value.split('')
        let newValue = []

        replace.map((v,i) => {

            if(mask[i] === "_"){
                newValue.push(v)
            } else {
                newValue.push(mask[i])
            }

        })

        setValueFormatted( newValue.join('') )

    }

    return(
        <>

            <label className="input-mask-tropa">

                { valueFormatted.length > 0 &&
                <input 
                    className="placeholder-mask" 
                    defaultValue={placeholderType}
                />
                }

                <input
                    className="value-mask"
                    onChange={handleOnChange}
                    placeholder={placeholder}
                    value={valueFormatted}
                />

            </label>

        </>
    )

}