import Icon from '@iconify/react'
import React, { useEffect, useState } from 'react'
import { api } from '../../Api/app'
import messageStore from '../../Store/MessageStore'
import EditList from '../ContactList/EditList'
import IconButton from '../Icon/IconButton'
import InputMaskTropa from '../InputMaskTropa'

const Lists = (props) => {

    const [load, setLoad] = useState(true)

    const [minhasListas, setMinhasListas] = useState([])

    const [idLista, setIdLista] = useState([])

    const [test, setTest] = useState('')

    useEffect(() => {

        console.log('chamada no step 3', props)

        api.get('contactLists?tipo='+props.send).then( response => {

            setMinhasListas( [...response.data.docs] )
            console.log( response.data.docs )

        })

    }, [props.send])

    useEffect(() => {

        var listasGeral = []
        var columnsGeral = []

        idLista.map((row) => {

            listasGeral.push( row._id )
            
            row.colunas.map((coluna) => {

                var exist = columnsGeral.filter( obj => obj === coluna ).length

                if ( exist === 0 ) {
                    columnsGeral.push(coluna)
                }

            })

        })

        props.handleOnChange( 'colunas', columnsGeral )
        props.handleOnChange( 'id_lista', listasGeral )

        console.log('alow')

    }, [idLista])

    function callbackChangeColumns(colunas, id)
    {

        idLista.map((row, key) => {

            if ( row._id === id ){
                idLista[key].colunas = colunas
            }

        })

        setIdLista([...idLista])

    }

    async function handleAddList( e )
    {

        const id = e.target.value

        if ( id !== 'all' ){

            const lista = minhasListas.filter( obj => 
                obj._id === id
            )[0]
            
            var listas = []
            idLista.map((row) => {
                listas.push(row)
            })
            listas.push( lista )
            setIdLista(listas)

            const removeLista = minhasListas.filter( obj => 
                obj._id !== id
            )

            setMinhasListas( removeLista )

        }

    }

    function handleRemoveList( id )
    {

        setLoadList(true)

        const lista = idLista.filter( obj => 
            obj._id === id
        )[0]
        var listas = []
        minhasListas.map((row) => {
            listas.push(row)
        })
        listas.push( lista )
        setMinhasListas(listas)
        const removeLista = idLista.filter( obj => 
            obj._id !== id
        )
        setIdLista( removeLista )

        setTimeout(function() {
            setLoadList(false)
        }, 10)

    }

    async function handleOnSave(e)
    {

        try {

            e.preventDefault()
            if ( idLista.length === 0 ) throw "Preencha pelo menos uma lista."
            props.setStep(props.next)

        } catch (e) {
            messageStore.addError(e)
        }
        
    }

    function callbackChangeName( id, name )
    {

        var listas = []
        
        idLista.map(row => {

            if ( row._id === id ){
                row.lista = name
            }

            listas.push(row)

        })

        setIdLista( listas )

    }

    function handleNewList( newList )
    {

        newList.contacts = 0
        minhasListas.push( newList )
        setMinhasListas([...minhasListas])

    }

    const [loadList, setLoadList] = useState(false)

    async function handleDelList( id )
    {

        const response = await api.delete('contactLists/'+id)

        const docs = idLista.filter( obj => 
            obj._id !== id
        )

        setIdLista([...docs])


    }

    return(
        <div className="step" style={{display: props.step === props.num ? 'block' : 'none'}}>

            <h2>{props.nome}</h2>
            <p>Selecione as pessoas ou listas que serão impactadas pela campanha.</p>

            <div className="separator">

                <div className="input">

                    <label>
                        <select 
                            name="id_lista"
                            type="text" 
                            className="input-default"
                            onChange={(e) => handleAddList(e)}
                            value="all"
                        >
                            <option value="all">Minhas listas salvas</option>
                            { minhasListas.length > 0 && minhasListas.map((row, key) => 
                                <option key={key} value={row._id}>{row.lista}</option>
                            )}
                        </select>
                    </label>

                    <div className="lists-options">

                        <EditList send={props.send} className="full" callbackChangeColumns={callbackChangeColumns} callbackNewList={(e) => handleNewList(e)}>
                            <button type="button" className="button-zig column full secondary">
                                <IconButton icon="new"/>
                                 <span>Criar nova lista</span>
                            </button>
                        </EditList>
                        
                    </div>

                    <div className="buttons">

                        { props.step !== 5 && <button onClick={handleOnSave}>Próximo passo</button>}
                        { props.step !== 1 && <button onClick={() => props.setStep( props.prev )} type="button">Passo anterior</button>}
                        
                    </div>
                </div>

                <div className="list">
                    
                    { loadList === false && idLista && idLista.map((row, key) => 
                        <div key={key}>

                            <button onClick={() => handleRemoveList(row._id)} type="button" style={{marginLeft: '10px'}} className="button-zig danger">
                                <IconButton icon="min"/>
                            </button>

                            <div>
                                <EditList send={props.send} callbackChangeColumns={callbackChangeColumns} id={row._id} callbackChangeName={callbackChangeName}>
                                    <span>{row.lista}</span><br/>
                                </EditList>
                            </div>

                            <EditList send={props.send} callbackChangeColumns={callbackChangeColumns} id={row._id} callbackChangeName={callbackChangeName}>
                                <button type="button" className="button-zig secondary">
                                    <IconButton icon="edit"/>
                                    <span>Editar</span>
                                </button>
                            </EditList>

                            <button onClick={() => messageStore.addConfirm('Tem certeza que deseja apagar a lista "'+row.lista+'"?', () => handleDelList(row._id))} type="button" style={{marginLeft: '10px'}} className="button-zig danger">
                                <IconButton icon="del"/>
                                <span>Apagar</span>
                            </button>

                        </div>
                    )}

                </div>

            </div>

        </div>
    )

}

export default Lists