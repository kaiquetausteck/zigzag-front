import React from 'react'
import Datetime from '../../Helper/Datetime'
import GoogleMapReact from 'google-map-react'

import messageStore from '../../Store/MessageStore'

export default function MessageRender({ 
    figurinha, 
    audio, 
    foto, 
    video,
    contatoJSON = {},
    localizacaoJSON = {},
    arquivo,
    message, 
    setImageZoom,
    resume = false,
    id
})
{

    const googleMap = {
        center: {
          lat: localizacaoJSON.latitude,
          lng: localizacaoJSON.longitude
        },
        zoom: 14
    }

    const AnyReactComponent = ({ text }) => <i style={{fontSize: 40, color: '#44a8ff'}} class="fa fa-map-marker" aria-hidden="true"></i>

    function detectURLs(message){

        var urlRegex = /(((https?:\/\/)|(www\.))[^\s]+)/g

        var links = message.match(urlRegex)

        console.log("LINKS ==========", links)

        return links !== null ? links[0] : false

    }

    function copyText(id)
    {

        var copyText = document.getElementById(id)
      
        copyText.select()
        copyText.setSelectionRange(0, 99999)
      
        document.execCommand("copy")
      
        messageStore.addSuccess(`${copyText.value} copiado.`)

    }

    return(
        <>
        
            { figurinha && 
                <div className="message-render">
                    { resume === true ?
                        <span className="center-message">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" width="16" height="16"><path fill="currentColor" d="M9.179 14.637c.061-.14.106-.29.135-.45.031-.171.044-.338.049-.543a9.05 9.05 0 0 0 .003-.233l.001-.067v-.072l.002-.216c.01-.364.032-1.205.08-1.473.052-.287.136-.538.255-.771a2.535 2.535 0 0 1 1.125-1.111 2.8 2.8 0 0 1 .786-.255c.27-.048 1.098-.07 1.487-.08l.152.001h.047l.325-.004a3.63 3.63 0 0 0 .554-.048 2.06 2.06 0 0 0 .494-.151 4.766 4.766 0 0 1-1.359 2.429 143.91 143.91 0 0 1-2.057 1.924 4.782 4.782 0 0 1-2.079 1.12zm-1.821.16l-.474.012a9.023 9.023 0 0 1-1.879-.11 4.747 4.747 0 0 1-1.314-.428 4.376 4.376 0 0 1-1.123-.807 4.354 4.354 0 0 1-.816-1.11 4.584 4.584 0 0 1-.434-1.303 8.783 8.783 0 0 1-.12-1.356 29.156 29.156 0 0 1-.009-.617c-.002-.206-.002-.37-.002-.736v-.674l.001-.549.001-.182c.001-.223.004-.426.009-.62a8.69 8.69 0 0 1 .121-1.358c.087-.476.229-.903.434-1.301a4.399 4.399 0 0 1 1.936-1.916 4.7 4.7 0 0 1 1.315-.429 8.926 8.926 0 0 1 1.379-.12c.72-.009.989-.011 1.359-.011h.528c.896.003 1.143.005 1.366.011.55.015.959.046 1.371.12.482.085.913.226 1.314.428a4.396 4.396 0 0 1 1.937 1.915c.206.4.348.827.434 1.302.075.412.107.819.121 1.356.006.198.009.402.01.619v.024c0 .069-.001.132-.003.194a2.61 2.61 0 0 1-.033.391.902.902 0 0 1-.494.677 1.05 1.05 0 0 1-.29.094 2.734 2.734 0 0 1-.395.033l-.311.004h-.039l-.163-.001c-.453.012-1.325.036-1.656.096a3.81 3.81 0 0 0-1.064.348 3.541 3.541 0 0 0-.911.655c-.267.263-.49.566-.661.899-.166.324-.281.67-.351 1.054-.06.33-.085 1.216-.096 1.636l-.002.23v.069l-.001.067c0 .074-.001.143-.003.213-.004.158-.014.28-.033.388a.902.902 0 0 1-.494.676 1.054 1.054 0 0 1-.289.093 1.335 1.335 0 0 1-.176.024z"></path></svg>
                            Figurinha
                        </span>
                        :
                        <div className="figure">
                            <img src={figurinha}/>
                        </div>
                    }
                </div>
            }

            { arquivo && 
                <div className="message-render">
                    { resume === true ?
                        <span className="center-message">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13 20" width="13" height="20"><path fill="currentColor" d="M10.2 3H2.5C1.7 3 1 3.7 1 4.5v10.1c0 .7.7 1.4 1.5 1.4h7.7c.8 0 1.5-.7 1.5-1.5v-10C11.6 3.7 11 3 10.2 3zm-2.6 9.7H3.5v-1.3h4.1v1.3zM9.3 10H3.5V8.7h5.8V10zm0-2.7H3.5V6h5.8v1.3z"></path></svg>
                            { message ? message : 'Arquivo' }
                        </span>
                        :
                        <a href={arquivo} target="_blank" className="archive">
                            <img src={'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAAA8CAYAAADL94L/AAAByElEQVR4Ae3axdJTQRAFYFyegA3u8ALseCDcicsGhxt3x+G32BXc3X3NBnfXYTqp3sZlhuqpOlXZRL46He9ReJyJxGSTEreaPfEHZiX+1uSJvelVNu+Jvjd7Yk9zI8aSUe0eDpjCIYfNSuw5v/zF5In/6mU27478tXriLJvXjdSwPq1lCDTCmxjiCNav8GZYBVMwWKagX8kWjk9vCcMhYWhEFEw1+oV0wZjdPKY6Vn9EwmBDTYPwBoXCYPLGDQTJjkHQNQRJj0FQtmgs+C8wOHIIkh2DoDu5vD5Xfkz9hsTBWDyxhjDYUDqvLRYSY1JilSQGyyxXOt4QKJPX70NDQmI27gyxHcn9bH/5RFMNAUgoDI4afOAMHBiCdiDNj5woGAhgsCEYudSI1lBCRwoPL957slAoDDYEoPXb/ZVs3FE/y9072fDxsx4BMPVfGOpl1VY/y5++4EWM1Fm9LcCKpy8RpnchDGEIQxjCEIYwhCEMYQhDGMIQhjCEIQxhCEMYwhCGMIQhDGEIQxhYNlXiP+XHXLRDM5thQVpyzIfS2YtLceVEkRmzalsgMArPhp258bA6b/LEb8LqPM930VNdvY/fhMmCxw+Of+4BTcPInBo2AAAAAElFTkSuQmCC'}/>
                            <span>Download arquivo</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34 34" width="34" height="34"><path fill="currentColor" d="M17 2c8.3 0 15 6.7 15 15s-6.7 15-15 15S2 25.3 2 17 8.7 2 17 2m0-1C8.2 1 1 8.2 1 17s7.2 16 16 16 16-7.2 16-16S25.8 1 17 1z"></path><path fill="currentColor" d="M22.4 17.5h-3.2v-6.8c0-.4-.3-.7-.7-.7h-3.2c-.4 0-.7.3-.7.7v6.8h-3.2c-.6 0-.8.4-.4.8l5 5.3c.5.7 1 .5 1.5 0l5-5.3c.7-.5.5-.8-.1-.8z"></path></svg>
                        </a>
                    }
                </div>
            }

            { audio && 
                <div className="message-render">
                    { resume === true ?
                        <span className="center-message">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 20" width="12" height="20"><path fill="currentColor" d="M6 11.745a2 2 0 0 0 2-2V4.941a2 2 0 0 0-4 0v4.803a2 2 0 0 0 2 2.001zm3.495-2.001c0 1.927-1.568 3.495-3.495 3.495s-3.495-1.568-3.495-3.495H1.11c0 2.458 1.828 4.477 4.192 4.819v2.495h1.395v-2.495c2.364-.342 4.193-2.362 4.193-4.82H9.495v.001z"></path></svg>
                            { message ? message : 'Áudio' }
                        </span>
                    :                 
                    <div className="audio">
                        <audio controls="controls">
                            <source src={audio} type="audio/mp3" />
                            seu navegador não suporta HTML5
                        </audio>
                    </div>
                    }
                </div>
            }

            { video && 
                <div className="message-render">
                    { resume === true ?
                        <span className="center-message">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 20" width="16" height="20"><path fill="currentColor" d="M15.243 5.868l-3.48 3.091v-2.27c0-.657-.532-1.189-1.189-1.189H1.945c-.657 0-1.189.532-1.189 1.189v7.138c0 .657.532 1.189 1.189 1.189h8.629c.657 0 1.189-.532 1.189-1.189v-2.299l3.48 3.09v-8.75z"></path></svg>
                            { message ? message : 'Vídeo' }
                        </span>
                    :                 
                    <div className="video">
                        <video controls="controls">
                            <source src={video} type="video/mp4" />
                            seu navegador não suporta HTML5
                        </video>
                    </div>
                    }
                </div>
            }

            { contatoJSON.nome && 
                <div className="message-render">
                    { resume === true ?
                        <span className="center-message">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 20" width="14" height="20"><path fill="currentColor" d="M6.844 9.975a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm5.759 3.087c-.884-.845-3.136-1.587-5.721-1.587-2.584 0-4.739.742-5.622 1.587-.203.195-.26.464-.26.746v1.679h12v-1.679c0-.282-.193-.552-.397-.746z"></path></svg>
                            { message ? message : contatoJSON.nome }
                        </span>
                    :                 
                    <div className="contact">
                        
                        <div className="name">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 212 212" width="212" height="212"><path fill="#DFE5E7" class="background" d="M106.251.5C164.653.5 212 47.846 212 106.25S164.653 212 106.25 212C47.846 212 .5 164.654.5 106.25S47.846.5 106.251.5z"></path><path fill="#FFF" class="primary" d="M173.561 171.615a62.767 62.767 0 0 0-2.065-2.955 67.7 67.7 0 0 0-2.608-3.299 70.112 70.112 0 0 0-3.184-3.527 71.097 71.097 0 0 0-5.924-5.47 72.458 72.458 0 0 0-10.204-7.026 75.2 75.2 0 0 0-5.98-3.055c-.062-.028-.118-.059-.18-.087-9.792-4.44-22.106-7.529-37.416-7.529s-27.624 3.089-37.416 7.529c-.338.153-.653.318-.985.474a75.37 75.37 0 0 0-6.229 3.298 72.589 72.589 0 0 0-9.15 6.395 71.243 71.243 0 0 0-5.924 5.47 70.064 70.064 0 0 0-3.184 3.527 67.142 67.142 0 0 0-2.609 3.299 63.292 63.292 0 0 0-2.065 2.955 56.33 56.33 0 0 0-1.447 2.324c-.033.056-.073.119-.104.174a47.92 47.92 0 0 0-1.07 1.926c-.559 1.068-.818 1.678-.818 1.678v.398c18.285 17.927 43.322 28.985 70.945 28.985 27.678 0 52.761-11.103 71.055-29.095v-.289s-.619-1.45-1.992-3.778a58.346 58.346 0 0 0-1.446-2.322zM106.002 125.5c2.645 0 5.212-.253 7.68-.737a38.272 38.272 0 0 0 3.624-.896 37.124 37.124 0 0 0 5.12-1.958 36.307 36.307 0 0 0 6.15-3.67 35.923 35.923 0 0 0 9.489-10.48 36.558 36.558 0 0 0 2.422-4.84 37.051 37.051 0 0 0 1.716-5.25c.299-1.208.542-2.443.725-3.701.275-1.887.417-3.827.417-5.811s-.142-3.925-.417-5.811a38.734 38.734 0 0 0-1.215-5.494 36.68 36.68 0 0 0-3.648-8.298 35.923 35.923 0 0 0-9.489-10.48 36.347 36.347 0 0 0-6.15-3.67 37.124 37.124 0 0 0-5.12-1.958 37.67 37.67 0 0 0-3.624-.896 39.875 39.875 0 0 0-7.68-.737c-21.162 0-37.345 16.183-37.345 37.345 0 21.159 16.183 37.342 37.345 37.342z"></path></svg>
                            <b>{ contatoJSON.nome }</b>
                        </div>

                        <div className="telefones">
                            { contatoJSON.telefones.map((row, key) =>
                                <label>
                                    <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><g fill="none"><path d="M8 4v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V7.242a2 2 0 0 0-.602-1.43L16.083 2.57A2 2 0 0 0 14.685 2H10a2 2 0 0 0-2 2z" stroke="#626262" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M16 18v2a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V9a2 2 0 0 1 2-2h2" stroke="#626262" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></g><rect x="0" y="0" width="24" height="24" fill="rgba(0, 0, 0, 0)" /></svg>
                                    <input readOnly={true} key={key} id={`contact-${row}-${id}`} value={row} onClick={() => copyText(`contact-${row}-${id}`)}/>
                                </label>
                            )} 
                        </div>
                    </div>
                    }
                </div>
            }

            { localizacaoJSON.latitude && 
                <div className="message-render">
                    { resume === true ?
                        <span className="center-message">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13 20" width="13" height="20"><path fill="currentColor" d="M6.487 3.305A4.659 4.659 0 0 0 1.8 7.992c0 3.482 4.687 8.704 4.687 8.704s4.687-5.222 4.687-8.704a4.659 4.659 0 0 0-4.687-4.687zm0 6.36c-.937 0-1.674-.737-1.674-1.674s.736-1.674 1.674-1.674 1.674.737 1.674 1.674c0 .938-.737 1.674-1.674 1.674z"></path></svg>
                            { message ? message : 'Localização' }
                        </span>
                    :                 
                    <div className="localizacao">
                        <GoogleMapReact
                            bootstrapURLKeys={{ key: 'AIzaSyDNnbvC6YiCzjPVTS2n1_S3OnLQXzXk49o' }}
                            defaultCenter={googleMap.center}
                            defaultZoom={googleMap.zoom}
                        >
                            <AnyReactComponent
                                lat={localizacaoJSON.latitude}
                                lng={localizacaoJSON.longitude}
                                text="My Marker"
                            />
                        </GoogleMapReact>
                    </div>
                    }
                </div>
            }

            { foto && 
                <div className="message-render">
                    { resume === true ?
                        <span className="center-message">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 20" width="16" height="20"><path fill="currentColor" d="M13.822 4.668H7.14l-1.068-1.09a1.068 1.068 0 0 0-.663-.278H3.531c-.214 0-.51.128-.656.285L1.276 5.296c-.146.157-.266.46-.266.675v1.06l-.001.003v6.983c0 .646.524 1.17 1.17 1.17h11.643a1.17 1.17 0 0 0 1.17-1.17v-8.18a1.17 1.17 0 0 0-1.17-1.169zm-5.982 8.63a3.395 3.395 0 1 1 0-6.79 3.395 3.395 0 0 1 0 6.79zm0-5.787a2.392 2.392 0 1 0 0 4.784 2.392 2.392 0 0 0 0-4.784z"></path></svg>
                            { message ? message : 'Foto' }
                        </span>
                    :
                        <div style={{backgroundImage: `url(${foto})`}} onClick={() => setImageZoom(foto)} className="photo"/>
                    }
                </div>
            }

            { resume === true && ( foto || video || arquivo || figurinha ) ? 
                <div></div>
            :
                <>
                    { resume === false && detectURLs(message).length >= 0 && 
                        <a href={(detectURLs(message).split('http').length === 1 ? 'https://' : '')+detectURLs(message)} target="_blank" className="link-interative">
                            <img src={'data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAA8AAD/4QMsaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA2LjAtYzAwMiA3OS4xNjQ0NjAsIDIwMjAvMDUvMTItMTY6MDQ6MTcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCAyMS4yIChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo1MUJFRjAxQjg4MTUxMUVCODVBMURDRUQ0RTkyQjMzQyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo1MUJFRjAxQzg4MTUxMUVCODVBMURDRUQ0RTkyQjMzQyI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjUxQkVGMDE5ODgxNTExRUI4NUExRENFRDRFOTJCMzNDIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjUxQkVGMDFBODgxNTExRUI4NUExRENFRDRFOTJCMzNDIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQABgQEBAUEBgUFBgkGBQYJCwgGBggLDAoKCwoKDBAMDAwMDAwQDA4PEA8ODBMTFBQTExwbGxscHx8fHx8fHx8fHwEHBwcNDA0YEBAYGhURFRofHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8f/8AAEQgBAAEAAwERAAIRAQMRAf/EAIYAAQADAAIDAQAAAAAAAAAAAAAHCAkCAwQFBgEBAQAAAAAAAAAAAAAAAAAAAAAQAAEDAgEECREIAgIDAAAAAAABAgMEBQYRIQcIMUFxEpJzFDcYYYGR0UJScqKyEzRUhKTUtVZRsTPTJJS0VSIygiOhFRYRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/ALUgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHi3W7Wy0W6e5XSqjoqClar6ipmcjI2NTbVygRs/Wg0FscrVxOiq1ci5KKvcmb7FSnVFA/OlHoJ+pvcbh8OA6Uegn6m9xuHw4DpR6Cfqb3G4fDgOlHoJ+pvcbh8OA6Uegn6m9xuHw4HbSazOg6qqI6eLFEbZJFRrXS0tbCxFXvpJYWManVcoEmwzRTRMmhe2SGRqPjkYqOa5rkyo5qpmVFQDkAAAAAAAAAAAAAAAAAAAAAAAAAAEMa2OHcSXvRYrLJFJUcirYqq4UsKK58lOxj2rkY3O7ePc167mXaAomqKiqipkVMyooH4AAAAAADQXVmq6iq0HYXlqJFkkbHVRNc5cqoyGtmijbuNYxGoBJ4AAAAAAAAAAAAAAAAAAAAAAAAAAAOlaGiVcq08Squ3vG9oD85BQ+rxcBvaAcgofV4uA3tAOQUPq8XAb2gK8axmn624YjnwlhFIX4jeisuFwY1jm0TXJnYxcmRZ1TgeFsBTz/tml7qSWR3Vc5znL2VVVA4AaAarnMThn275hUASoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAV41jdY2PDMdRhHCU6PxFIix3C4MVFbRNVM7GLtzr4m7sBTf9RVVHdz1M7+q973vXrq5zlUC4WgDVzZhi2ri7FsDX4jfC99vt70RzaJrmLke9NhZ1TgeFsBTgDQDVc5icM+3fMKgCVAAAAAAAAAAAAA4+cjTuk7KAPOx9+nZQB52Pv07KAPOx9+nZQB52Pv07KAPOx9+nZQB52Pv07KAPOx9+nZQB52Pv07KAPOx9+nZQDkioqZUXKn2oAAAAAFeNY3WNjwzHUYRwjUI/ET0WO4XBioraJqpnYxdudU4HhbAU2/UVVR3c9RO/qve97166uc5VAuTq5auUWGY4MW4up0kxFIiPt9vemVtE1dh702518Td2AsBX+g1HFP8AJUDLIDQDVc5icM+3fMKgCVAAAAAAAAAACP8ATBpmw5o0szait/V3iqRf/W2qNyI+VUzK96595G1dl2TqJlApjjnWA0oYwqZFqrvLb6Fyqsdut7nU8LWrl/xcrF378y925QI8lllmkWSV7pJHZ3PcquVd1VA4AAAAAAAAAAFh9TjFWJmY8nw7HPLPYaijlnnpXK50UL4lbvJWIuZiqrt6uTZy9RALmgAAFeNY3WNjwzHUYRwlOj8RPRWXC4MVFbRNVM7GLtzr4m7sBTf9RVVHdz1M7+q973vXrq5zlUC5Grlq5R4Zjp8XYup2vxE9Ekt9veiKlE1UzPemws6+J4WwFhwOiv8AQajin+SoGWQGgGq5zE4Z9u+YVAEqAAAAAAAAAPEu10o7Taqy6Vr0jo6GGSoqJF2GxxNV7l7CAZs6Qsb3TG2LrhiK4vVZKuRfMQquVIYGrkiib9iNb/5yrtgSJoM1crppEiW9XSofa8MRvWNs8bUWepe1cj2wb7K1rWrmV6oufNkXPkCxtLqnaE4IGxy2moqntREWaWsqUc7qqkb429hoHd0VdB39DJ+9rfzgHRV0Hf0Mn72t/OAdFXQd/Qyfva384B0VdB39DJ+9rfzgHRV0Hf0Mn72t/OAdFXQd/Qyfva384B0VdB39DJ+9rfzgHRV0Hf0Mn72t/OAdFXQd/Qyfva384D7PA+jPA+B6eaDDFqjoOUZOUTb58s0m9yqiOllV71RMuZMuQD6cABXjWN1jY8Mx1GEcJVDX4ie1Y7hcGLlSiaqZ2MXYWdU4HhbAU3/UVVR3c9TO/qve97166uc5VAuRq5auUeGY6fF2LYEfiJ6JJb7e9EVtE1UzPem3OvibuwFhwAHRX+g1HFP8lQMsgNANVzmJwz7d8wqAJUAAAAAAAAARprJV81DoRxTNCuR74YIFVO9qKqKF6ddsigZ7Aad4MsdHYcJWezUbEZT0FHDCxE296xN85eq52VVX7QPcgAAAAAAAAAAAAArxrG6xseGY6jCWEahH4ieisuFwjXK2iaqZ2MXbnXxN3YCm36iqqO7nqJ39V73vevXVznKoFydXLVyjwzHT4txdTo/ET0R9vt8iIraJqpme9NudfE8LYCw4AAB01yKtFUImdVifk4KgZZKioqoqZFTMqKBoBqvsezQXhhHIrVVK1ci/Y6vqFReuigSmAAAAAAAAAivWj5icTew/MKcDP8DU2g9Bp+KZ5KAd4AAAAAAAAAAAAV41jdY2PDMdRhHCU6PxE9FjuFwYqK2iaqZ2MXbnXxPC2Apv+oqqju56md/Ve973r11c5yqBcjVy1co8Mx0+LsWwI/ET0SS3296IraJqpme9NhZ18TwtgLDgAAAABDOItU7RXe8SSXt6VtHyiRZqq30srGU8j3LvnLkcxz2b5dpjk6gEuWq1W+0WyltltgbS0FFE2Gmp40yNZGxMjWoB5QAAAAAAAACK9aPmJxN7D8wpwM/wNTaD0Gn4pnkoB3gAAAAAAAAAACvGsbrGx4ZjqMJYRqEfiJ6Ky4XCNUVtE1UzsYu3OvieFsBTf9RVVHdz1M7+q973vXrq5zlUC5Grlq5R4Zjgxbi6nR+Inoj7fb5ERW0TVTM96bc6+J4WwFhwAAAAAAAAAAAAAAAAABFetHzE4m9h+YU4Gf4GptB6DT8UzyUA7wAAAAAAAAACvGsbrGx4ZjqMI4SnR+IpEWO4XBioraJqpnYxdudfE3dgKb/qKqo7uepnf1Xve969dXOcqgXI1ctXKPDMdPi7FtO1+Inoklvt70RUomqmZ702PPr4nhbAWHAAfMaTccU+B8D3XE80PKeQRt8zT5d6kk0r2xRNVURciK96ZV+wCrOFdcfHjMTQSYiho6iwzyo2qp4IlifDE52RXxP3zlVWJnyOy5epsgXNRUVEVNhc6AAAAAAAAAAAAAAARXrR8xOJvYfmFOBn+BqbQeg0/FM8lAO8AAAAAAAABXnWO1jGYYZUYRwlOj8RvarLhcGLlSia5M7GLtzqnA8LYCm36iqqO7nqZ39V73vevXVznKoFyNXLVyiwzHT4uxbTpJiKREfb7e9EVtE1UzPem3OvibuwFhwAACJdarmOv3GUX82ECgoGqUX4bNxPuA5AAAAAAAAAAAAAAivWj5icTew/MKcDP8DU2g9Bp+KZ5KAd4AAAAAAAADO3Sfoux9ZMeXKjrbXV1klbVyzUdZDFJMyqbLIrmvY5qOyuXLnbsooFjNXPVxZhhkGLcXU7X4jeiPt9vfkc2iRdh702FnXxPC2AsMAAAAIl1quY6/cZRfzYQKCgapRfhs3E+4DkAAAAAAAAAAAAACK9aPmJxN7D8wpwM/wNTaD0Gn4pnkoB3gAAAAAAAAAAAAAAAIl1quY6/cZRfzYQKCgapRfhs3E+4DkAAAAAAAAAAAAACK9aPmJxN7D8wpwM/wADU2g9Bp+KZ5KAd4AAAAAAAAAAAAAAACJdarmOv3GUX82ECgoGqUX4bNxPuA5AAAAAAAAAAAAAAivWj5icTew/MKcDP8DU2g9Bp+KZ5KAd4AAAAAAAAAAAAAAACJdarmOv3GUX82ECgoGqUX4bNxPuA5AAAAAAAAAAAAAAivWj5icTew/MKcDP8DU2g9Bp+KZ5KAd4AAAAAAAAAAAAAAACJdarmOv3GUX82ECgoGqUX4bNxPuA5AAAAAAAAAAAAAAivWj5icTew/MKcDP8DU2g9Bp+KZ5KAd4AAAAAAAAAAAAAAACJdarmOv3GUX82ECgoGqUX4bNxPuA5AAAAAAAAAAAAAAivWj5icTew/MKcDP8AA1NoPQafimeSgHeAAAAAAAAAAAAAAAAiXWq5jr9xlF/NhAoKBqlF+GzcT7gOQAAAAAAAAAAAAAIr1o+YnE3sPzCnAz/A1NoPQafimeSgHeAAAAAAAAAAAAAAAAiXWq5jr9xlF/NhAoKBqlF+GzcT7gOQAAAAAAAAAAAAAIu1nopJdBmJ2sTK5G0j1RPsZXQOcvWRAM/ANR7JVQ1lmoKuByPgqKaKWJ6LlRWvYjmqnWUDzQAAAAAAAAAAAAAAAEP62NXBBoTu0Ujka+qqKOKFFX/Z6VLJVRP+MblAodDFJNKyKNN9JI5GMamyquXIiAanxpkjanUT7gOQAAAAAAAAAAAAAPTYzw5BibCd3w/Ou9judJLTb/vXSMVGv/4uyKBmheLTX2e61dquESwV1DM+nqYnJkVr43K1yZ9wC0GrfrIWOhsdLgzGdUlDyFPNWm7SrkhWHuYZndx5vYa5f8d7mXJkzhZukvdmrIGz0lfT1ED0RWSxSse1UXbRzVVAO7l9D6xFw29sBy+h9Yi4be2A5fQ+sRcNvbAcvofWIuG3tgOX0PrEXDb2wHL6H1iLht7YDl9D6xFw29sBy+h9Yi4be2A5fQ+sRcNvbAcvofWIuG3tgOX0PrEXDb2wHL6H1iLht7YHrb7jTCNho31l5vFHQU7Eyq+aZjVXqNbl3zl+xETKBSvWN05xaRbpTWuyo+PDFre58DpEVr6mdU3vn3MX/VrW5UYi5865dnIgfP6v+BqnF+lC0UqRq6gt8rbhcZMmVrYqdyPRrs2T/sfvWdcDQ0AAAAAAAAAAAAAAABAesVq6/wD2m/xRhdjIsTxMRKukVUYytYxMjf8AJczZmpmRVzOTMu0BTO72a7Wevlt91o5qGugVWy01Qx0b2qi5P9XIgHhgAAAAAAAAAAAAAAAPpMEaPMX42ujbdh23yVb1VEmqMitghRe6llX/ABaiZd37EUC+GhrQ/ZtGuHOQ07kqrvV72S63LJkWV6JmYxFztjZlXep11zqBIAAAAAAAAAAAAAAAAAB6bEeDMJ4lgSC/2ikucbf9OUxMkc3aytcqb5vWUD4ebVi0GSyK92GGo5dlGVdcxOs1s6IBw6Lmgn6Z9+uHxADouaCfpn364fEAOi5oJ+mffrh8QA6Lmgn6Z9+uHxADouaCfpn364fEAOi5oJ+mffrh8QA6Lmgn6Z9+uHxADouaCfpn364fEAOi5oJ+mffrh8QA6Lmgn6Z9+uHxADouaCfpn364fEAOi5oJ+mffrh8QB5NBq26EaGZJoMLQPemdEqJqqob12TyyNXsASDbLTa7VRsorZRw0NJGmSOnp42xRtRPsaxEQDygAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf/9k='}/>
                            <div className="text">
                                <span>
                                    Link interativo
                                </span>
                                <p>Clique aqui para acessar o link</p>
                                <i>{detectURLs(message)}</i>
                            </div>
                            
                        </a> 
                    }
                    <div className="message-render" dangerouslySetInnerHTML={{__html: message}}/>
                </>
            }

        </>
    )

}