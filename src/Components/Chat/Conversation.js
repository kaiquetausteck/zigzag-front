import React, { useEffect, useRef, useState } from 'react'
import { api } from '../../Api/app'
import Messages from './Messages'
import SendMessage from './SendMessage'
import { Scrollbars } from 'react-custom-scrollbars'
import { Lightbox } from "react-modal-image"

export default function Conversation({conversation, updateContacts, tab})
{

    const [load, setLoad] = useState(true)

    const [messages, setMessages] = useState([])

    const [scrollPosition, setScrollPosition] = useState(false)

    const [imageZoom, setImageZoom] = useState(false)

    const refScroll = useRef()

    useEffect(() => {

        if ( load === false ) {
            
            setTimeout(() => {

                var element = document.getElementById(conversation.id)
    
                if ( element ) {
                    refScroll.current.scrollTop(element.offsetTop)
                }
                
            }, 1000)

        }

    }, [load])

    useEffect(() => {

        console.log( scrollPosition )

        if ( scrollPosition >= 1 || scrollPosition === false ) {

            handleScrollToBottom()

        }

    }, [scrollPosition, messages, load])

    useEffect(() => {

        setLoad(true)
        setScrollPosition(false)
        getMessages()

        let rotationInterval = setInterval(() => {

            getMessages()
           
        }, 5000)
        
        return () => {

            setLoad(true)
            console.log('getMessages, clearInterval')
            clearInterval(rotationInterval)

        }

    }, [conversation])

    async function getMessages()
    {

        try {
            
            const responseMessages = []

            const response = await api.get('conversations/'+conversation.queueId+'/whatsapp')

            response.data.map((row, key) => {

                responseMessages.push({
                    id: row.id,
                    messageText: row.mensagem,
                    messageDate: new Date(row.createdAt),
                    status: row.statusPadrao,
                    figurinha: row.figurinha,
                    audio: row.audio,
                    foto: row.imagem,
                    video: row.video,
                    contatoJSON: row.contatoJSON,
                    localizacaoJSON: row.localizacaoJSON,
                    arquivo: row.arquivo,
                    type: row.chatbotId || row.userId || row.campanha ? 'sent' : 'received',
                    chatbotId: row.chatbotId,
                    author: row.userId || row.chatbotId ? row.nome : false,
                    nome: row.nome,
                    campanha: row.campanha
                })

                if ( key === response.data.length - 1 && row.createdAt !== conversation.dateMessage ) {

                    sendMessage({
                        id: row.id,
                        type: row.chatbotId || row.userId ? 'sent' : 'received',
                        messageText: row.mensagem,
                        messageDate: row.createdAt,
                        status: row.statusPadrao,
                        figurinha: row.figurinha,
                        audio: row.audio,
                        video: row.video,
                        contatoJSON: row.contatoJSON,
                        localizacaoJSON: row.localizacaoJSON,
                        foto: row.imagem,
                        arquivo: row.arquivo,
                        type: row.userId ? 'sent' : 'received',
                        chatbotId: row.chatbotId,
                        author: row.userId || row.chatbotId ? row.nome : false,
                        nome: row.nome,
                        campanha: row.campanha
                    })
                }

            })

            setMessages([...responseMessages])
            setLoad(false)

        } catch ( err ){

            console.log(err)

        }

    }

    async function sendMessage( messageResponse )
    {

        conversation.audio = messageResponse.audio
        conversation.video = messageResponse.video
        conversation.contatoJSON = messageResponse.contatoJSON
        conversation.localizacaoJSON = messageResponse.localizacaoJSON
        conversation.figurinha = messageResponse.figurinha 
        conversation.foto = messageResponse.foto
        conversation.arquivo = messageResponse.arquivo
        conversation.lastMessage = messageResponse.messageText
        conversation.dateMessage = messageResponse.messageDate
        conversation.type = messageResponse.type
        conversation.chatbotId = messageResponse.chatbotId
        conversation.author = messageResponse.author

        updateContacts( conversation )

        messages.push(messageResponse)
        setMessages([...messages])

        if ( messageResponse.type === 'sent' ) {

            setTimeout(() => {
                handleScrollToBottom()
            }, 10)
        }

    }

    async function updateStatusMessage( messageResponse )
    {

        const newMessages = []

        messages.map((row) => {
            if ( row.messageDate === messageResponse.messageDate ) {
                messageResponse.status = 'Enviado'
                newMessages.push(messageResponse)
            } else {
                newMessages.push(row)
            }
        })

        setMessages([...newMessages])

    }

    function handleScrollToBottom()
    {

        refScroll.current.scrollToBottom()


    }

    return(
        <div id="conversation">

            { imageZoom && 
                <Lightbox
                    small={imageZoom}
                    large={imageZoom}
                    onClose={() => setImageZoom(false)}
                    hideDownload={true}
                />
            }

            <div className="relative">
                <div className="header">

                    <span>{ conversation.phone }</span>

                </div>

                <Scrollbars 
                    ref={refScroll}
                    onScrollFrame={(e) => e.top !== 0 && setScrollPosition(e.top)}
                    className="messages" 
                    id={"scroll-element"}
                >
                    <Messages
                        messages={messages}
                        setLoad={setLoad}
                        conversation={conversation}
                        load={load}
                        setImageZoom={setImageZoom}
                        
                    />
                </Scrollbars>

                { scrollPosition !== false && scrollPosition < 1 && 
                    <div className="button-scroll">
                        <button className={`scroll-to-bottom`} onClick={() => refScroll.current.scrollToBottom()}>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 20" width="19" height="20"><path fill="currentColor" d="M3.8 6.7l5.7 5.7 5.7-5.7 1.6 1.6-7.3 7.2-7.3-7.2 1.6-1.6z"></path></svg>
                        </button>
                    </div>
                }
                
                <SendMessage
                    conversation={conversation}
                    sendMessage={sendMessage}
                    updateStatusMessage={updateStatusMessage}
                    handleScrollToBottom={handleScrollToBottom}
                    tab={tab}
                />

            </div>

        </div>
    )

}   