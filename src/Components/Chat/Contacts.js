import React, { useEffect, useState } from 'react'
import Datetime from '../../Helper/Datetime'
import MessageRender from './MessageRender'
import { Scrollbars } from 'react-custom-scrollbars'
import Search from './Search'

export default function Contacts({
    contacts, 
    setContacts, 
    conversation, 
    setConversation,
    load
})
{

    const [data, setData] = useState(contacts)

    useEffect(() => {

        setData([...contacts])

    }, [contacts])

    const Contact = ({row}) => {

        return(
            <div onClick={() => setConversation(row)} className={`contact ${conversation.queueId === row.queueId ? 'active' : ''}`}>

                <div className="person">
                    <b>{row.phone}</b>
                    <div className={`message ${row.totalMessage > 0 && row.queueId !== conversation.queueId ? 'active' : ''}`}>
                        
                        <MessageRender
                            message={row.type === 'sent' ? 'Enviado: '+row.lastMessage : row.lastMessage}
                            audio={row.audio}
                            video={row.video}
                            arquivo={row.arquivo}
                            figurinha={row.figurinha}
                            contatoJSON={row.contatoJSON}
                            localizacaoJSON={row.localizacaoJSON}
                            foto={row.foto}
                            resume={true}
                        />
                    </div>
                </div>

                <div className="infos">
                    <span className="date">
                        {Datetime.string(row.dateMessage)}
                    </span>
                    <span className={`totalMessage ${row.totalMessage === 0 || row.queueId === conversation.queueId ? 'hidden' : 'visible'}`}>
                        { row.queueId === conversation.queueId ? 0 : row.totalMessage}
                    </span>
                </div>

            </div>
        )
    }

    return(
        <>
            
            <div id="contacts">
                <Scrollbars 
                    style={{ width: '100%', height: '100%' }}
                >

                    { load === true &&
                        <div className="loading-general">
                            <div>
                                <i className="fa fa-spinner fa-spin"></i>
                            </div>
                        </div>
                    }

                    { load === false && data.map((row, key) =>
                        <Contact row={row} key={key}/>
                    )}
                </Scrollbars>

            </div>
        </>
    )

}