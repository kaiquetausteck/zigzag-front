import React from 'react'

export default function Profile({profile})
{

    return(
        <div id="profile">

            <div className="photo" style={{backgroundImage: `url(${profile.imagem})`}}></div>
            <h1>{profile.nome}</h1>

        </div>
    )

}