import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

//scss
import './LayoutPrivate.scss'
//images
import Logo from '../../Images/logo.png'
import MaleSupport from '../../Images/male_support.png'
//images_icons
import IconDashboard from '../../Images/icos/icon_dashboard.png'
import IconCampgain from '../../Images/icos/icon_campgain.png'
import IconTemplate from '../../Images/icos/icon_template.png'
import IconAnalytics from '../../Images/icos/icon_analytics.png'
import IconConfiguration from '../../Images/icos/icon_configuration.png'
import IconTicket from '../../Images/icos/icon_ticket.png'
import IconFaq from '../../Images/icos/icon_faq.png'
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min'

import { Icon, InlineIcon } from '@iconify/react'
import usersLight from '@iconify-icons/ph/users-light'
import sendAlt from '@iconify-icons/carbon/send-alt'
import { api } from '../../Api/app'

export default function LayoutPrivate(props){

    const history = useHistory()

    const [page, setPage] = useState('/'+props.location.pathname.split('/')[1])
    const [pageTwo, setPageTwo] = useState('/'+props.location.pathname.split('/')[2])
    const [backend, setBackend] = useState({})

    useEffect(() => {

        setPage('/'+props.location.pathname.split('/')[1])
        setPageTwo('/'+props.location.pathname.split('/')[2])
        scrollTop()
        getVersionBackend()

    }, [props])

    function scrollTop(){
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        })
    }

    async function getVersionBackend()
    {

        try {

            const response = await api.get('')
            setBackend({...response.data})

        } catch ( err ) {

            console.log('não foi possivel resgatar o backend.')

        }

    }

    return(
        <div id="zigzag">

            <nav className="nav-left">

                <div className="logo">
                    {/* <img alt="Logo" src={Logo}/> */}
                    <span style={{width: '100%', textAlign: 'center', display: 'flex', justifyContent: 'center', color: '#FFF', fontSize: 30}}>text2you</span>
                </div>

                {/* <div className="link">
                    <Link className={page === '/nova-campanha' ? 'active' : 'no-active'} to="/nova-campanha">
                        <b>+</b>
                        <span>Nova campanha</span>
                    </Link>
                </div> */}

                <ul>

                    <li>
                        <Link className={page === '/' ? 'active' : 'no-active'} to="/">
                            <img alt="Icone dashboard" src={IconDashboard}/>
                            <span className="text">Dashboard</span>
                            <span className="bg"></span>
                        </Link>
                    </li>

                    <li style={{marginTop: '10px'}}>
                        <span>Envios</span>
                    </li>

                    <li>
                        <Link className={page === '/nova-campanha' && pageTwo === '/Whatsapp' ? 'active' : 'no-active'} to="/nova-campanha/Whatsapp">
                        <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 256 256"><path d="M128 28a100.026 100.026 0 0 0-86.885 149.54l-9.005 31.516a12 12 0 0 0 14.835 14.834l31.517-9.004A100.007 100.007 0 1 0 128 28zm0 192a91.87 91.87 0 0 1-46.952-12.867a3.995 3.995 0 0 0-3.144-.408l-33.157 9.473a4 4 0 0 1-4.944-4.945l9.472-33.156a4.001 4.001 0 0 0-.408-3.144A92.01 92.01 0 1 1 128 220zm50.512-73.457l-20.46-11.691a12.01 12.01 0 0 0-12.127.129l-13.807 8.284a44.042 44.042 0 0 1-19.382-19.383l8.284-13.807a12.01 12.01 0 0 0 .128-12.127l-11.69-20.46A10.916 10.916 0 0 0 100 72a32.008 32.008 0 0 0-32 31.88A84.001 84.001 0 0 0 151.999 188h.12A32.008 32.008 0 0 0 184 156a10.913 10.913 0 0 0-5.488-9.457zM152.108 180H152a76 76 0 0 1-76-76.107A23.997 23.997 0 0 1 100 80a2.9 2.9 0 0 1 2.512 1.457l11.69 20.461a4.004 4.004 0 0 1-.042 4.042l-9.39 15.648a3.999 3.999 0 0 0-.218 3.699a52.041 52.041 0 0 0 26.142 26.141a3.997 3.997 0 0 0 3.699-.218l15.647-9.39a4.006 4.006 0 0 1 4.043-.043l20.46 11.692A2.897 2.897 0 0 1 176 156a23.997 23.997 0 0 1-23.892 24z" fill="#626262"/><rect x="0" y="0" width="256" height="256" fill="rgba(0, 0, 0, 0)" /></svg>
                            <span className="text">Enviar Whatsapp</span>
                            <span className="bg"></span>
                        </Link>
                    </li>

                    <li>
                        <Link className={page === '/nova-campanha' && pageTwo === '/SMS' ? 'active' : 'no-active'} to="/nova-campanha/SMS">
                            <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><path d="M3 20.586L6.586 17H18a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2H5a2 2 0 0 0-2 2v14.586zM3 22H2V6a3 3 0 0 1 3-3h13a3 3 0 0 1 3 3v9a3 3 0 0 1-3 3H7l-4 4zM6.5 9a1.5 1.5 0 1 1 0 3a1.5 1.5 0 0 1 0-3zm0 1a.5.5 0 1 0 0 1a.5.5 0 0 0 0-1zm5-1a1.5 1.5 0 1 1 0 3a1.5 1.5 0 0 1 0-3zm0 1a.5.5 0 1 0 0 1a.5.5 0 0 0 0-1zm5-1a1.5 1.5 0 1 1 0 3a1.5 1.5 0 0 1 0-3zm0 1a.5.5 0 1 0 0 1a.5.5 0 0 0 0-1z" fill="#626262"/><rect x="0" y="0" width="24" height="24" fill="rgba(0, 0, 0, 0)" /></svg>
                            <span className="text">Enviar SMS</span>
                            <span className="bg"></span>
                        </Link>
                    </li>

                    <li>
                        <Link className={page === '/nova-campanha' && pageTwo === '/Email' ? 'active' : 'no-active'} to="/nova-campanha/Email">
                        <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><path d="M5 5h13a3 3 0 0 1 3 3v9a3 3 0 0 1-3 3H5a3 3 0 0 1-3-3V8a3 3 0 0 1 3-3zm0 1c-.488 0-.936.175-1.283.466L11.5 11.52l7.783-5.054A1.992 1.992 0 0 0 18 6H5zm6.5 6.712L3.134 7.28A1.995 1.995 0 0 0 3 8v9a2 2 0 0 0 2 2h13a2 2 0 0 0 2-2V8c0-.254-.047-.497-.134-.72L11.5 12.711z" fill="#626262"/><rect x="0" y="0" width="24" height="24" fill="rgba(0, 0, 0, 0)" /></svg>
                            <span className="text">Enviar E-mail</span>
                            <span className="bg"></span>
                        </Link>
                    </li>

                    <li style={{marginTop: '10px'}}>
                        <span>Complementos de envios</span>
                    </li>

                    <li>
                        <Link className={page === '/chat' ? 'active' : 'no-active'} to="/chat/whatsapp">
                        <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 256 256"><path d="M128 28a100.026 100.026 0 0 0-86.885 149.54l-9.005 31.516a12 12 0 0 0 14.835 14.834l31.517-9.004A100.007 100.007 0 1 0 128 28zm0 192a91.87 91.87 0 0 1-46.952-12.867a3.995 3.995 0 0 0-3.144-.408l-33.157 9.473a4 4 0 0 1-4.944-4.945l9.472-33.156a4.001 4.001 0 0 0-.408-3.144A92.01 92.01 0 1 1 128 220zm50.512-73.457l-20.46-11.691a12.01 12.01 0 0 0-12.127.129l-13.807 8.284a44.042 44.042 0 0 1-19.382-19.383l8.284-13.807a12.01 12.01 0 0 0 .128-12.127l-11.69-20.46A10.916 10.916 0 0 0 100 72a32.008 32.008 0 0 0-32 31.88A84.001 84.001 0 0 0 151.999 188h.12A32.008 32.008 0 0 0 184 156a10.913 10.913 0 0 0-5.488-9.457zM152.108 180H152a76 76 0 0 1-76-76.107A23.997 23.997 0 0 1 100 80a2.9 2.9 0 0 1 2.512 1.457l11.69 20.461a4.004 4.004 0 0 1-.042 4.042l-9.39 15.648a3.999 3.999 0 0 0-.218 3.699a52.041 52.041 0 0 0 26.142 26.141a3.997 3.997 0 0 0 3.699-.218l15.647-9.39a4.006 4.006 0 0 1 4.043-.043l20.46 11.692A2.897 2.897 0 0 1 176 156a23.997 23.997 0 0 1-23.892 24z" fill="#626262"/><rect x="0" y="0" width="256" height="256" fill="rgba(0, 0, 0, 0)" /></svg>
                            <span className="text">Chat whatsapp</span>
                            <span className="bg"></span>
                        </Link>
                    </li>

                    <li>
                        <Link className={page === '/chatbot' ? 'active' : 'no-active'} to="/chatbot">
                            <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 15 15"><g fill="none"><path d="M9 2.5V2v.5zm-3 0V3v-.5zm6.856 9.422l-.35-.356l-.205.2l.07.277l.485-.12zM13.5 14.5l-.121.485a.5.5 0 0 0 .606-.606l-.485.12zm-4-1l-.354-.354l-.624.625l.857.214l.121-.485zm.025-.025l.353.354a.5.5 0 0 0-.4-.852l.047.498zM.5 8H0h.5zM7 0v2.5h1V0H7zm2 2H6v1h3V2zm6 6a6 6 0 0 0-6-6v1a5 5 0 0 1 5 5h1zm-1.794 4.279A5.983 5.983 0 0 0 15 7.999h-1a4.983 4.983 0 0 1-1.495 3.567l.701.713zm.78 2.1L13.34 11.8l-.97.242l.644 2.578l.97-.242zm-4.607-.394l4 1l.242-.97l-4-1l-.242.97zm-.208-.863l-.025.024l.708.707l.024-.024l-.707-.707zM9 14c.193 0 .384-.01.572-.027l-.094-.996A5.058 5.058 0 0 1 9 13v1zm-3 0h3v-1H6v1zM0 8a6 6 0 0 0 6 6v-1a5 5 0 0 1-5-5H0zm6-6a6 6 0 0 0-6 6h1a5 5 0 0 1 5-5V2zm1.5 6A1.5 1.5 0 0 1 6 6.5H5A2.5 2.5 0 0 0 7.5 9V8zM9 6.5A1.5 1.5 0 0 1 7.5 8v1A2.5 2.5 0 0 0 10 6.5H9zM7.5 5A1.5 1.5 0 0 1 9 6.5h1A2.5 2.5 0 0 0 7.5 4v1zm0-1A2.5 2.5 0 0 0 5 6.5h1A1.5 1.5 0 0 1 7.5 5V4zm0 8c1.064 0 2.042-.37 2.813-.987l-.626-.78c-.6.48-1.359.767-2.187.767v1zm-2.813-.987c.77.617 1.75.987 2.813.987v-1a3.483 3.483 0 0 1-2.187-.767l-.626.78z" fill="#626262"/></g><rect x="0" y="0" width="15" height="15" fill="rgba(0, 0, 0, 0)" /></svg>
                            <span className="text">Chatbot whatsapp</span>
                            <span className="bg"></span>
                        </Link>
                    </li>

                    <li>
                        <Link className={page === '/templates' ? 'active' : 'no-active'} to="/templates">
                            <img alt="Icone template" src={IconTemplate}/>
                            <span className="text">Landing pages</span>
                            <span className="bg"></span>
                        </Link>
                    </li>

                    <li style={{marginTop: '10px'}}>
                        <span>Gerenciamento</span>
                    </li>

                    <li>
                        <Link className={page === '/campanhas' ? 'active' : 'no-active'} to="/campanhas">
                            <img alt="Icone campanha" src={IconCampgain}/>
                            <span className="text">Minhas Campanhas</span>
                            <span className="bg"></span>
                        </Link>
                    </li>

                    {/* <li>
                        <Link className={page === '/templates' ? 'active' : 'no-active'} to="/templates/">
                            <img alt="Icone template" src={IconTemplate}/>
                            <span className="text">Templates</span>
                            <span className="bg"></span>
                        </Link>
                    </li> */}

                    <li>
                        <Link className={page === '/analytics' ? 'active' : 'no-active'} to="/analytics">
                            <img alt="Icone analytics" src={IconAnalytics}/>
                            <span className="text">Visualizar envios</span>
                            <span className="bg"></span>
                        </Link>
                    </li>

                    <li>
                        <Link className={page === '/configuracoes' ? 'active' : 'no-active'} to="/configuracoes">
                            <img alt="Icone configurações" src={IconConfiguration}/>
                            <span className="text">Configurações</span>
                            <span className="bg"></span>
                        </Link>
                    </li>

                    {/* <li>
                        <span>Suporte</span>
                    </li> */}

                    <li style={{display: 'none'}}>
                        <Link className={page === '/tickets' ? 'active' : 'no-active'} to="/tickets">
                            <img alt="Icone Ticket" src={IconTicket}/>
                            <span className="text">Tickets</span>
                            <span className="bg"></span>
                        </Link>
                    </li>

                    <li style={{display: 'none'}}>
                        <Link className={page === '/faq' ? 'active' : 'no-active'} to="/faq">
                            <img alt="Icone FAQ" src={IconFaq}/>
                            <span className="text">FAQ</span>
                            <span className="bg"></span>
                        </Link>
                    </li>

                    { props.user.isAdmin &&
                    <>
                        <li>
                            <span>Administração</span>
                        </li>

                        <li>
                            <Link className={page === '/adm-contas' ? 'active' : 'no-active'} to="/adm-contas">
                                
                                    <Icon icon={usersLight} />
                                
                                <span className="text">Gerenciar contas</span>
                                <span className="bg"></span>
                            </Link>
                        </li>

                        <li>
                            <Link className={page === '/adm-brokers' ? 'active' : 'no-active'} to="/adm-brokers">
                                
                                    <Icon icon={sendAlt} />
                                
                                <span className="text">Gerenciar brokers</span>
                                <span className="bg"></span>
                            </Link>
                        </li>
                    </>
                    }

                </ul>

                <div className="suport" style={{display: 'none'}}>
                    <Link className={page === '/faq' ? 'active' : 'no-active'} to="/faq">

                        <span className="title">Centro de Ajuda</span>
                        <span className="description">Visite o nosso centro de ajuda</span>
                        <span className="link">Visitar</span>

                        <img alt="Suporet" src={MaleSupport}/>

                    </Link>
                </div>

                <span className="version" style={{color:'#FFF', fontSize:11, paddingBottom: 10, display: 'flex', justifyContent: 'center', alignItems: 'center'}}>v{backend.Version}</span>

            </nav>

            <section className="section-private">

                <header>

                    <div className="left">
                        <div className="name-client">
                            {props.user.account.nome}
                        </div>
                    </div>

                    <div className="right">

                        <div className="notification">
                            <button onClick={() => history.push('/notificacoes')}>
                            
                            { props.logs.total > 0 &&
                                <span id="total-notification">{props.logs.total}</span>
                            }

                            </button>
                        </div>

                        <div className="menu-user">
                            
                            <button>
                                <i id="img-profile" style={{backgroundImage: 'url('+props.user.foto+')'}}></i>
                                <span id="name-user">{props.user.nome}</span>
                            </button>

                            <div className="submenu">

                                <Link to="/configuracoes">Configurações</Link>
                                {/* <Link to="/configuracoes/dados-pagamento">Dados de cobrança</Link> */}
                                <Link to="/notificacoes">Notificações</Link>
                                <Link to="/login">Sair</Link>

                            </div>

                        </div>

                    </div>

                </header>

                <div className="content">

                    {props.children}

                </div>

            </section>

        </div>
    )

}