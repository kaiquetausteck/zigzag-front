import React from 'react'
import datesFormater from '../../Helper/DatesFormater'

export default function Answers({respostas, origem})
{

    return(
        <div className={`answers-align ${origem}`}>
            <div className={`display ${origem}`}>
                <div className={`answers ${origem}`}>
                    { respostas.map((row, key) =>
                        <div className="answer">
                            {row.resposta}
                            <span>{datesFormater.dateBR(row.createdAt)}</span>
                        </div>
                    )}
                </div>
            </div>
        </div>
    )

}